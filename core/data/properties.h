
//==================================================
// @title        parameters.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <tuple>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * TODO: comment
 */
struct ErrorProperties
{
	double  abs_tol, rel_tol;
	bool    norm_control;

	ErrorProperties() { set_defaults(); }


	inline void set_tolerance  ( double absolute, double relative )
		{ abs_tol=absolute; rel_tol=relative; }
	inline void set_defaults   (void)
		{ abs_tol=1e-8; rel_tol=1e-6; norm_control=false; }

	inline void set_norm_control  () { norm_control=true;  }
	inline void unset_norm_control() { norm_control=false; }
};

// ------------------------------------------------------------------------

/**
 * TODO: comment
 */
template <class _time>
struct StepProperties
{
	using time_type = _time;

	time_type  init_step, min_step, max_step;
	usize_t    num_steps;

	inline void set_step     ( time_type dt )
		{ set_step( dt, dt/100, dt*100 ); }
	inline void set_step     ( time_type dt, time_type dtmin, time_type dtmax )
		{ init_step=dt; min_step=dtmin; max_step=dtmax; }
	inline void set_num_steps ( usize_t n )
		{ num_steps = n; }
};

// ------------------------------------------------------------------------

/**
 * TODO: comment
 */
template <class _idata>
struct Events
{
	typedef _idata                 data_type;
	typedef dr::signal<data_type>  signal_type;

	signal_type
		after_init, before_step, after_step, after_commit;
};

// ------------------------------------------------------------------------

/**
 * The metric is used for error-control (default: osd::default_metric).
 */
template <class _idata, class _metric>
struct Properties
{
	using idata_type   = _idata;
	using metric_type  = _metric;
	using time_type    = typename idata_type::time_type;
	using err_type     = double;

	StepProperties<time_type>  step;
	ErrorProperties            error;
	Events<idata_type>         event;
	metric_type                metric;

	bool check() const
	{
		ASSERT_RF( step.min_step >= dr::c_num<time_type>::eps,
			"[prop.step] Min step is too small." );

		ASSERT_RF( step.init_step >= step.min_step,
			"[prop.step] Initial step should be greater than min step." );

		ASSERT_RF( step.max_step >= step.init_step,
			"[prop.step] Max step should be greater than initial step." );

		ASSERT_RF( error.abs_tol > dr::c_num<err_type>::eps,
			"[prop.error] Absolute tolerance is too small." );

		ASSERT_RF( error.rel_tol > dr::c_num<err_type>::eps,
			"[prop.error] Relative tolerance is too small." );

		return true;
	}
};

OSD_NS_END_
