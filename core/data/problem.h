
//==================================================
// @title        problem.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Initial value problems are a type of integration problem given a function
 * to integrate (here a system capable of computing its derivative) and an
 * initial state of the system.
 *
 * This data structure simply translates to: "integrate a given system between
 * times t0 and t1 assuming that the state of the system at t0 is x0".
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time>
struct IntegrationProblem
{
	using pool_type   = Pool<_value,_time>;
	using value_type  = _value;
	using time_type   = _time;

	time_type t_start, t_end;
	usize_t   b_size; // block-size for memory pool

	// ----------  =====  ----------

	IntegrationProblem()
		: b_size(OSD_BLOCK_SIZE) {}

	// Time-interval
	inline time_type time_interval() const
		{ return t_end-t_start; }

	// What is the size of the problem?
	virtual usize_t state_size() const =0;

	// Initialise records before integration
	virtual bool init( pool_type& records ) const =0;
};

// ------------------------------------------------------------------------

template <class _value, class _time = double>
struct InitialValueProblem
	: public IntegrationProblem<_value,_time>
{
	using pool_type   = Pool<_value,_time>;
	using value_type  = _value;
	using time_type   = _time;

	cref_vector<value_type> x_start; // constant reference vector

	// ----------  =====  ----------

	InitialValueProblem()
		{ clear(); }

	void clear()
	{
		this->t_start = this->t_end = 0.0;
		x_start.clear();
	}

	inline usize_t state_size() const
		{ return x_start.size(); }

	bool init( pool_type& records ) const
	{
		// resize memory pool
		records.init( state_size(), this->b_size );
		ASSERT_RF( records, "[InitialValueProblem.init] Memory pool initialisation failed." );

		// record initial time and state
		records.time () = this->t_start;
		records.state() .copy(x_start);

		return true;
	}
};

// ------------------------------------------------------------------------

template <class _value, class _time = double>
struct HistoryProblem
	: public IntegrationProblem<_value,_time>
{
	using pool_type   = Pool<_value,_time>;
	using value_type  = _value;
	using time_type   = _time;

	TimeSeries<const value_type,const time_type> ts;

	// ----------  =====  ----------

	HistoryProblem()
		{ clear(); }

	inline usize_t state_size() const
		{ return ts.state_size(); }

	void clear()
	{
		this->t_start = this->t_end = 0.0;
		ts.clear();
	}

	bool init( pool_type& records ) const
	{
		// resize memory pool
		records.init( state_size(), this->b_size );
		ASSERT_RF( records, "[HistoryProblem.init] Memory pool initialisation failed." );

		// record history
		for ( usize_t i = 0; i < ts.size(); ++i )
		{
			records.time () = ts.time(i);
			records.state() .copy( ts.state(i) );
			records.increment();
		}
		records.decrement();

		return true;
	}
};

OSD_NS_END_
