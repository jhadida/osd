
//==================================================
// @title        idata.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * TODO: comment
 */

#include <stdexcept>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _system, class _timepoint>
struct IntegrationData
{
	typedef _system     system_type;
	typedef _timepoint  timepoint_type;

	using time_type     = typename system_type::time_type;
	using value_type    = typename system_type::value_type;
	using pool_type     = typename system_type::pool_type;
	using problem_type  = IntegrationProblem<value_type,time_type>;

	// ----------  =====  ----------

	system_type    *sys;
	problem_type   *prob;
	timepoint_type  cur, next;
	double          err;

	// ----------  =====  ----------

	// bind input system and allocate timepoint storage
	bool init( system_type& s, problem_type& p );

	void clear  ();
	void commit (); // copy non-allocated timepoint data from next to cur

	// Validity of the current state
	inline bool valid() const
	{
		return sys && prob && err >= 0.0 &&
			cur.size() == next.size() &&
			cur.size() == sys->state_size();
	}

	// Proxy to system properties
	inline usize_t     size    () const { return sys? sys->state_size() : 0; }
	inline pool_type&  records () const { return sys->records; }

	// Proxy to problem properties
	inline time_type t_start () const { return prob->t_start; }
	inline time_type t_end   () const { return prob->t_end; }
	inline time_type t_span  () const { return t_end() - t_start(); }

	// Adjust current time-step to finish exactly at t_end
	inline void adjust_time_step()
		{ cur.dt = std::min( cur.dt, dr::op_abs(prob->t_end - cur.t) ); }

	// True if cur.t is past the last time in the given problem
	inline bool done() const
		{ return cur.t >= prob->t_end; }
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class S, class T>
void IntegrationData<S,T>::clear()
{
	sys  = nullptr;
	prob = nullptr;
	err  = 0.0;

	cur  .clear();
	next .clear();
}

// ------------------------------------------------------------------------

template <class S, class T>
bool IntegrationData<S,T>::init( system_type& s, problem_type& p )
{
	sys  = &s;
	prob = &p;
	err  = 0.0;

	// Allocate storage for timepoints
	cur  .resize( s.state_size() );
	next .resize( s.state_size() );

	return valid();
}

// ------------------------------------------------------------------------

template <class S, class T>
void IntegrationData<S,T>::commit()
{
	// Copy only non-allocated data
	cur.t     = next.t;
	cur.dt    = next.dt;
	cur.x     = next.x; // "pointer" to pool vector (cf dr::array)
	cur.index = next.index;
}

OSD_NS_END_
