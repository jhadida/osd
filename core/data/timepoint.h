
//==================================================
// @title        timepoint.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * A timepoint is a data-structure that stores the state of the system x and
 * its derivative with respect to time dxdt evaluated at a given time t.
 *
 * The timepoint can also store the Jacobian J of a system if needed, and stores
 * the timestep dt that was used when the derivative/Jacobian were computed, as
 * well as the index of that time-point in the memory pool.
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time = double>
struct TimePoint
{
	using traits       = tp_traits<_value,_time>;
	using value_type   = _value;
	using time_type    = _time;
	using state_type   = typename traits::state_type;
	using deriv_type   = typename traits::deriv_type;

	usize_t     index;
	time_type   t, dt;
	state_type  x;
	deriv_type  dxdt;

	// ----------  =====  ----------

	TimePoint()
		{ clear(); }

	explicit
	TimePoint( usize_t s_size )
		{ resize(s_size); }


	virtual void clear()
	{
		index=0; t=dt=0.0;
		x    .clear();
		dxdt .clear();
	}

	inline virtual void resize( usize_t s_size )
		{ dxdt.create(s_size); }


	inline usize_t size       () const { return state_size(); }
	inline usize_t state_size () const { return dxdt.size(); }

	// WARNING
	//   The following methods are implemented for convenience.
	//   They assume a fixed timestep dt; if an adaptive timestep integrator is used,
	//   there will be a differente between these methods and the actual previous/next
	//   times saved in the memory pool.
	inline time_type prev_time() const { return t - dt; }
	inline time_type next_time() const { return t + dt; }
};

// ------------------------------------------------------------------------

template <class _value, class _time>
struct TimePoint_Jacobian
	: public TimePoint<_value,_time>
{
	using traits         = tp_traits<_value,_time>;
	using parent         = TimePoint<_value,_time>;
	using value_type     = _value;
	using time_type      = _time;
	using state_type     = typename traits::state_type;
	using deriv_type     = typename traits::deriv_type;
	using jacobian_type  = typename traits::jacobian_type;

	jacobian_type  dfdx; // Jacobian matrix
	deriv_type     dfdt; // non-stationarity

	// ----------  =====  ----------

	TimePoint_Jacobian()
		{ clear(); }

	explicit
	TimePoint_Jacobian( usize_t s_size )
		{ resize(s_size); }


	void clear()
	{
		parent::clear();
		dfdx.clear();
		dfdt.clear();
	}

	void resize( usize_t s_size )
	{
		parent::resize(s_size);
		dfdt.create(s_size,0);
		if ( dfdx.ncols() != s_size || dfdx.nrows() != s_size )
			dfdx = dr::create_matrix<value_type>(s_size,s_size);
	}
};

OSD_NS_END_
