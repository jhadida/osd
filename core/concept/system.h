
//==================================================
// @title        system.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * System objects implement methods to evaluate the rhs of an ODE system in canonical
 * form (assuming the system can be reduced to a set of first order equations only).
 *
 * If/when necessary, the system may also provide a implementation of the Jacobian,
 * either using the analytical formulation or a numerical approximation. By default,
 * a computationally expensive central approximation is used to compute the Jacobian.
 *
 * Any realistic system will finally come with parameters, which may alter the values
 * or even the form of the equations. Make sure your system is fully parameterized
 * before you give it to the integrator for solving your initial value problem, by
 * defining your own configuration methods in the derived classes.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time = double>
struct System_base
{
	// Public access to the records
	Pool<_value,_time> records;

	inline virtual void clear() { records.clear(); }

	// Dimensions
	inline usize_t size       () const { return records.size(); }
	inline usize_t n_states   () const { return size(); }               // # of recorded states
	inline usize_t state_size () const { return records.state_size(); } // # of coordinates per state
};

// ------------------------------------------------------------------------

template <class _value, class _time = double>
struct System
	: public System_base<_value,_time>
{
	using self           = System<_value,_time>;
	using traits         = tp_traits<_value,_time>;
	using time_type      = _time;
	using value_type     = _value;
	using pool_type      = Pool<_value,_time>;
	using state_type     = typename pool_type::state_type;
	using deriv_type     = state_type; // implicit conversion from allocated vector to vector reference
	using jacobian_type  = typename traits::jacobian_type;

	// ----------  =====  ----------

	/**
	 * Canonical systems must provide a method to compute the derivative at a given timepoint.
	 *
	 * Input timepoint members  : t, x
	 * Output timepoint members : dxdt
	 */
	virtual void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const =0;

	/**
	 * All systems must provide a method to compute the Jacobian at a given timepoint.
	 * By default, the NumericJacobian is used to compute each element of the Jacobian
	 * as a numeric estimate of the partial derivative using forward approximation,
	 * which is slow (it requires n evaluations of the derivative). You can use central
	 * approximation instead by setting the OSD_NUMJAC_METHOD flag (cf osd.h), but it is
	 * even slower (2n evaluations of the derivative).
	 *
	 * If you are using a stepping scheme that requires the Jacobian, try to provide
	 * your own implementation for computing it (eg derived from analytics).
	 *
	 * Input timepoint members  : t, x
	 * Output timepoint members : dfdx (matrix), dfdt (vector)
	 */
	virtual void jacobian( time_type t, const state_type& x, const jacobian_type& dfdx, const deriv_type& dfdt ) const
	{
		dr::fill( dfdt.iter(), value_type(0) );
		m_numjac( *this, t, x, dfdx );
	}

protected:

	mutable NumericJacobian<value_type,time_type> m_numjac;
};

// ------------------------------------------------------------------------

template <class _value, class _time = double>
struct ItoSystem
	: public System<_value,_time>
{
	using self        = ItoSystem<_value,_time>;
	using traits      = tp_traits<_value,_time>;
	using time_type   = _time;
	using value_type  = _value;
	using pool_type   = Pool<_value,_time>;
	using state_type  = typename pool_type::state_type;
	using deriv_type  = state_type;

	/**
	 * Ito systems are of the form:
	 *   dX = Drift(t,X) dt + Volatility(t,X) dW
	 *
	 * Where W is a Wiener process.
	 */
	virtual void drift      ( time_type t, const state_type& x, const deriv_type& dxdt ) const =0;
	virtual void volatility ( time_type t, const state_type& x, const deriv_type& dxdw ) const =0;

	// By default the derivative returns the drift component
	inline void derivative( time_type t, const state_type& x, const deriv_type& dxdt ) const
		{ drift( t, x, dxdt ); }
};

OSD_NS_END_
