
//==================================================
// @title        stepper.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Stepper objects implement the various mathematical and numerical approaches to
 * computing a single time-step in dynamical systems described by differences (eg
 * Euler, Midpoint, Runge-Kutta, etc.).
 *
 * If/when possible, the stepper may be able to estimate the error of its prediction,
 * in which case it should be returned by the step function. Otherwise, the output of
 * the step function can be anything, typically 0.0.
 *
 * Finally, some steppers may also suggest an optimal timestep to integrator objects
 * in addition to / instead of the time-step suggested by the controller. This can be
 * done via the timepoint field state.next.dt. In either case, the integrator should
 * have the final word with regards to the chosen time-step.
 */

#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <
	class    _system, // (cf System): implements the derivative & Jacobian
	uint8_t  _order,  // maximum derivative order used for stepping
	bool     _error,  // whether the stepper estimates prediction error
	bool     _jac = false, // whether it requires the Jacobian information
	class    _metric = default_metric<typename _system::value_type> // metric used for error control
>
class Stepper
{
public:

	typedef _system  system_type;
	typedef _metric  metric_type;

	using order            = std::integral_constant<uint8_t,_order>;
	using estimates_error  = dr::bool_type<_error>;
	using traits           = typename system_type::traits;
	using time_type        = typename system_type::time_type;
	using value_type       = typename system_type::value_type;
	using state_type       = typename system_type::state_type;
	using problem_type     = IntegrationProblem< value_type, time_type >;

	// These types should be overriden depending on the needs of the stepper
	using timepoint_type =
		typename std::conditional< _jac,
			TimePoint_Jacobian< value_type, time_type >,
			TimePoint< value_type, time_type >
		>::type;

	using idata_type    = IntegrationData< system_type, timepoint_type >;
	using property_type = Properties< idata_type, metric_type >;

	// ----------  =====  ----------

	// Clear contents
	virtual void clear() {}

	// Stepping method to be overloaded
	virtual double step( idata_type& idata, property_type& prop ) =0;

	// Set initial derivative
	//
	// NOTE: overload this method to set the Jacobian if needed
	virtual bool init( idata_type& idata, property_type& prop )
	{
		auto& tp  =  idata.cur;
		auto& sys = *idata.sys;

		pool_fetch_timepoint( sys.records, tp );
		tp.dt = prop.step.init_step;
		sys.derivative( tp.t, tp.x, tp.dxdt );

		return true;
	}

};

OSD_NS_END_
