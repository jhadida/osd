
//==================================================
// @title        plugin.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Plugins are built to listen to integrator events (see Properties.Events).
 * This is useful typically when constraints must be enforced on the states
 * independently from the state equations (eg, range constraints), or when
 * intermediary or auxiliary information must be saved, eg for monitoring or
 * analysis purposes.
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _idata>
class Plugin
{
public:

	using self        = Plugin<_idata>;
	using idata_type  = _idata;
	using system_type = typename idata_type::system_type;
	using slot_type   = dr::slot<idata_type>;
	using signal_type = dr::signal<idata_type>;

	// ----------  =====  ----------

	Plugin() {}

	inline void attach( signal_type& sig )
	{
		m_slot = sig.subscribe(
			[this]( idata_type& s ){ this->callback(s); }
		);
	}
	inline void detach( signal_type& sig )
		{ sig.unsubscribe( m_slot ); }

protected:

	// The callback function is called at each publishing event
	virtual void callback( idata_type& idata ) =0;

	// The slot is waiting for publications on the attached channel
	slot_type m_slot;
};

OSD_NS_END_
