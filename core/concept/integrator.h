
//==================================================
// @title        integrator.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Integrator objects propagate the system between two time instants, typically
 * given an initial state. At the moment, only Initial Value Problems are supported.
 *
 * Integrators may or may not leverage the fact that the underlying stepper provides
 * an estimation of its error to locally adapt the time-step.
 *
 * They essentially serve as an intermediary between the system that can compute the
 * state and derivatives at a given timepoint, and the stepper that propagates the
 * state of the system from one timepoint to the next.
 */



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _stepper>
class Integrator
{
public:

	typedef Integrator<_stepper>  self;
	typedef _stepper              stepper_type;

	using time_type      = typename stepper_type::time_type;
	using value_type     = typename stepper_type::value_type;
	using system_type    = typename stepper_type::system_type;
	using problem_type   = typename stepper_type::problem_type;
	using property_type  = typename stepper_type::property_type;
	using idata_type     = typename stepper_type::idata_type;

	// ----------  =====  ----------

	// Store the stepper as a public member
	stepper_type stepper;

	Integrator()
		{ clear(); }

	inline virtual void clear()
	{
		m_idata.clear();
		stepper.clear();
	}

	virtual void integrate( system_type& sys, problem_type& prob, property_type& prop ) =0;


protected:

	// Data used to propagate the system during integration
	idata_type m_idata;

	// Initialise the system before integration
	bool init( system_type& sys, problem_type& prob, property_type& prop );

	/**
	 * Fetch is called before a step and, if the step is accepted the integrator (cf error control),
	 * it is then committed into the records.
	 *
	 * This means that while the stepper is at work, the latest timestep in the pool (aka "next" in idata)
	 * is one step ahead of the "present" (aka cur.t). This is important to know if you need to interpolate
	 * past records to compute e.g. delayed terms, because the last timepoint in the records should be ignored.
	 */
	void commit_next()
	{
		pool_commit_timepoint( m_idata.records(), m_idata.next );
		m_idata.commit();
	}

	void fetch_next()
	{
#ifdef OSD_USING_MATLAB
		dr::mx_throw_on_interruption("Interruption detected.",OSD_MATLAB_INTERRUPT_N);
#endif
		m_idata.records().increment();
		pool_fetch_timepoint( m_idata.records(), m_idata.next );
	}

};

// ------------------------------------------------------------------------

template <class S>
bool Integrator<S>::init( system_type& sys, problem_type& prob, property_type& prop )
{
	// Quick check-up of inputs
	ASSERT_RF( prop.check(),
		"[properties] Invalid properties." );
	ASSERT_RF( prob.time_interval() >= dr::c_num<time_type>::eps,
		"[problem] Time-interval is too small." );

	// Initialise memory pool
	//  - resize the pool to the dimensions of inital state(s)
	//  - record initial conditions depending on problem to solve
	//  - make sure initial state-size and system state-size match
	ASSERT_RF( prob.init( sys.records ), "[records] Could not initialise memory pool." );
	ASSERT_RF( prob.state_size() == sys.state_size(), "[system] Size mismatch between initial conditions and system state-size." );

	// Initialise integration data
	//  - bind to system and problem being solved
	//  - allocate timepoints
	ASSERT_RF( m_idata.init( sys, prob ), "[idata] Failed to initialise." );

	// Initialise the stepper
	//  - bind "current" timepoint to the last timepoint in records
	//  - call system to set its derivative (and Jacobian if needed)
	ASSERT_RF( stepper.init( m_idata, prop ), "[stepper] Failed to initialise." );

	return true;
}

OSD_NS_END_
