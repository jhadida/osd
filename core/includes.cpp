
//==================================================
// @title        includes.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Interaction with Matlab
#ifdef OSD_USING_MATLAB
    #include "matlab/io.cpp"
#endif
