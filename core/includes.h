#ifndef OSD_CORE_H_INCLUDED
#define OSD_CORE_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Data
#include "data/problem.h"
#include "data/timepoint.h"
#include "data/idata.h"
#include "data/properties.h"

// Interaction with Matlab
#ifdef OSD_USING_MATLAB
    #include "matlab/timeseries.h"
    #include "matlab/io.h"
#endif

// Concepts
#include "concept/system.h"
#include "concept/stepper.h"
#include "concept/integrator.h"
#include "concept/controller.h"
#include "concept/plugin.h"

#endif
