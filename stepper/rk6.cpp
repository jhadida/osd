
//==================================================
// @title        rk6.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Based on:
// http://www.ams.org/journals/mcom/1968-22-102/S0025-5718-68-99876-1/S0025-5718-68-99876-1.pdf



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

const double rk6_21 = -std::sqrt(21.0);
const double rk6_nu = 1.0; // arbitrary in (0,1), recommended value: 1

// k2 = f(k1)
const double rk6_constants::w21 = rk6_nu;
const double rk6_constants::t2  = rk6_nu;

// k3 = f(k1,k2)
const double rk6_constants::w31 = (4*rk6_nu-1) / (8*rk6_nu);
const double rk6_constants::w32 = 1.0 / (8*rk6_nu);
const double rk6_constants::t3  = 0.5;

// k4 = f(k1,k2,k3)
const double rk6_constants::w41 = (10*rk6_nu-2) / (27*rk6_nu);
const double rk6_constants::w42 = 2.0 / (27*rk6_nu);
const double rk6_constants::w43 = 8.0 / 27.0;
const double rk6_constants::t4  = 2.0 / 3.0;

// k5 = f(k1,k2,k3,k4)
const double rk6_constants::w51 = ( 8.0*(7+rk6_21) - (77+17*rk6_21)*rk6_nu ) / (392*rk6_nu);
const double rk6_constants::w52 = -8.0*(7+rk6_21) / (392*rk6_nu);
const double rk6_constants::w53 = 48.0*(7+rk6_21)*rk6_nu / (392*rk6_nu);
const double rk6_constants::w54 = -3.0*(21+rk6_21)*rk6_nu / (392*rk6_nu);
const double rk6_constants::t5  = (7+rk6_21) / 14.0;

// k6 = f(k1,k2,k3,k4,k5)
const double rk6_constants::w61 = 5.0*( 8.0*(7-rk6_21) - (287-59*rk6_21)*rk6_nu ) / (1960*rk6_nu);
const double rk6_constants::w62 = -40.0*(7-rk6_21) / (1960*rk6_nu);
const double rk6_constants::w63 = 320*rk6_21*rk6_nu / (1960*rk6_nu);
const double rk6_constants::w64 = 3.0*(21-121*rk6_21)*rk6_nu / (1960*rk6_nu);
const double rk6_constants::w65 = 392.0*(6-rk6_21)*rk6_nu / (1960*rk6_nu);
const double rk6_constants::t6  = (7-rk6_21) / 14.0;

// k7 = f(k1,k2,k3,k4,k5,k6)
const double rk6_constants::w71 = 15.0*( (30-7*rk6_21)*rk6_nu - 8.0 ) / (180*rk6_nu);
const double rk6_constants::w72 = 120.0 / (180*rk6_nu);
const double rk6_constants::w73 = -40.0*(5+7*rk6_21)*rk6_nu / (180*rk6_nu);
const double rk6_constants::w74 = 63.0*(2+3*rk6_21)*rk6_nu / (180*rk6_nu);
const double rk6_constants::w75 = -14.0*(49-9*rk6_21)*rk6_nu / (180*rk6_nu);
const double rk6_constants::w76 = 70.0*(7+rk6_21)*rk6_nu / (180*rk6_nu);

// Order 6 estimate = f(k1,k3,k5,k6,k7)
const double rk6_constants::a1 = 9.0 / 180.0;
const double rk6_constants::a3 = 64.0 / 180.0;
const double rk6_constants::a5 = 49.0 / 180.0;
const double rk6_constants::a6 = 49.0 / 180.0;
const double rk6_constants::a7 = 9.0 / 180.0;

OSD_NS_END_
