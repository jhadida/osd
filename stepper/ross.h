
//==================================================
// @title        ross.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>

// Base on:
// http://www.unige.ch/~hairer/prog/stiff/rodas.f



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

struct Ross_constants
{
    static const double
        gam,
        t2, t3, t4,
        d1, d2, d3, d4,
        w21,
        w31, w32,
        w41, w42, w43,
        w51, w52, w53, w54,
        z21,
        z31, z32,
        z41, z42, z43,
        z51, z52, z53, z54,
        z61, z62, z63, z64, z65;
};

// ------------------------------------------------------------------------

template <class _system>
struct Stepper_Ross
	: public Stepper<_system,4,true,true>, public Ross_constants
{
    typedef Stepper_Ross<_system>         self;
	typedef Stepper<_system,4,true,true>  parent;

	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
    using property_type  = typename parent::property_type;
    using idata_type     = typename parent::idata_type;

    // ----------  =====  ----------

    void clear()
	{
        stepid = 0;
		k1.clear();
		k2.clear();
		k3.clear();
		k4.clear();
		k5.clear();
		k6.clear();
		kdata.clear();
		yerr.clear();
	}

    double step( idata_type& d, property_type& prop )
    {
        ASSERT_RVAL( d.valid(), 0.0, "Invalid state.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;

		// ----------  =====  ----------
		// Part 1: Evaluate K-points

		// Prepare k1 through k6 arrays
		resize(dim);

		auto h  = d.cur.dt;
		auto x  = d.next.x;
        auto dx = d.next.dxdt;

        // compute only once per step, regardless of number of trials
        if ( stepid != d.next.index )
        {
            sys.derivative( d.cur.t, d.cur.x, d.cur.dxdt );
            sys.jacobian( d.cur.t, d.cur.x, d.cur.dfdx, d.cur.dfdt );

            // copy Jacobian matrix
            for ( usize_t i=0; i<dim; i++ )
            for ( usize_t j=0; j<dim; j++ )
                LU.lu(i,j) = -d.cur.dfdx(j,i); // algorithm assumes transpose matrix

            stepid = d.next.index;
        }

        // LU decomposition
        for ( usize_t i=0; i<dim; i++ )
            LU.lu(i,i) = 1.0/(gam*h) - d.cur.dfdx(i,i);
        LU.decompose();

        // K1
        algebra::iter_apply_3( x, d.cur.dxdt, d.cur.dfdt,
			algebra::weighted_sum_2<double>( 1.0, h*d1 ) );
        LU.solve(x,k1);

        algebra::iter_apply_3( x, d.cur.x, k1,
			algebra::weighted_sum_2<double>( 1.0, w21 ) );
        sys.derivative( d.cur.t + h*t2, x, dx );

        // K2
        algebra::iter_apply_4( x, dx, d.cur.dfdt, k1,
			algebra::weighted_sum_3<double>( 1.0, h*d2, z21/h ) );
        LU.solve(x,k2);

        algebra::iter_apply_4( x, d.cur.x, k1, k2,
			algebra::weighted_sum_3<double>( 1.0, w31, w32 ) );
        sys.derivative( d.cur.t + h*t3, x, dx );

        // K3
        algebra::iter_apply_5( x, dx, d.cur.dfdt, k1, k2,
			algebra::weighted_sum_4<double>( 1.0, h*d3, z31/h, z32/h ) );
        LU.solve(x,k3);

        algebra::iter_apply_5( x, d.cur.x, k1, k2, k3,
			algebra::weighted_sum_4<double>( 1.0, w41, w42, w43 ) );
        sys.derivative( d.cur.t + h*t4, x, dx );

        // K4
        algebra::iter_apply_6( x, dx, d.cur.dfdt, k1, k2, k3,
            algebra::weighted_sum_5<double>( 1.0, h*d4, z41/h, z42/h, z43/h ) );
        LU.solve(x,k4);

        algebra::iter_apply_6( x, d.cur.x, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54 ) );
        sys.derivative( d.cur.t + h, x, dx );

        // K5
        algebra::iter_apply_6( k6, dx, k1, k2, k3, k4,
            algebra::weighted_sum_5<double>( 1.0, z51, z52, z53, z54, 1.0/h ) );
        LU.solve(k6,k5);

        x += k5; sys.derivative( d.cur.t + h, x, dx );

        // Compute the solution and the error
        algebra::iter_apply_7( k6, dx, k1, k2, k3, k4, k5,
            algebra::weighted_sum_6<double>( 1.0, z61, z62, z63, z64, z65, 1.0/h ) );

        LU.solve(k6,yerr); x += yerr;


        // ----------  =====  ----------
		// Part 2: Evaluate error

        double error, scale, delta;
        error = 0.0;
        for ( usize_t i = 0; i < dim; ++i )
        {
            scale = std::max( std::abs(d.cur.x[i]), std::abs(d.next.x[i]) );
            // scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
            scale = prop.error.abs_tol + prop.error.rel_tol * scale;

            delta = yerr[i]/scale;
            error += delta*delta;
        }

        return sqrt( error / dim );
    }

private:

	void resize( usize_t n )
	{
		if ( yerr.size() != n )
		{
			yerr .create(n);
			kdata.create(6*n);
            LU.resize(n);

			k1.assign( &kdata[0*n], n );
			k2.assign( &kdata[1*n], n );
			k3.assign( &kdata[2*n], n );
			k4.assign( &kdata[3*n], n );
			k5.assign( &kdata[4*n], n );
            k6.assign( &kdata[5*n], n );
		}
	}

    usize_t stepid;
	dr::vector<value_type>  yerr, kdata;
	dr::array <value_type>  k1, k2, k3, k4, k5, k6;
    LUDecomposition<value_type> LU;
};

OSD_NS_END_
