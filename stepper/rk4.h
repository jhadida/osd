
//==================================================
// @title        rk4.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _system>
struct Stepper_RK4
	: public Stepper<_system,4,false>
{
	typedef Stepper_RK4<_system>      self;
	typedef Stepper<_system,4,false>  parent;

	// Forward types
	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	void clear()
	{
		k1.clear();
		k2.clear();
		k3.clear();
		k4.clear();
		kdata.clear();
	}

	double step ( idata_type& d, property_type& prop )
	{
		ASSERT_RVAL( d.valid(), 0.0, "Invalid state.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;

		// Prepare k1 through k4 arrays
		resize(dim);
		k1 = d.cur.dxdt;

		auto h = d.cur.dt;
		auto x = d.next.x;

		// K1
		sys.derivative( d.cur.t, d.cur.x, k1 );

		// K2
		algebra::iter_apply_3( x, d.cur.x, k1, algebra::weighted_sum_2<double>( 1.0, h/2 ) );
		sys.derivative( d.cur.t + h/2, x, k2 );

		// K3
		algebra::iter_apply_3( x, d.cur.x, k2, algebra::weighted_sum_2<double>( 1.0, h/2 ) );
		sys.derivative( d.cur.t + h/2, x, k3 );

		// K4
		algebra::iter_apply_3( x, d.cur.x, k3, algebra::weighted_sum_2<double>( 1.0, h ) );
		sys.derivative( d.cur.t + h, x, k4 );

		// Final estimate
		d.next.t = d.cur.t + h;
		algebra::iter_apply_6( d.next.x, d.cur.x, k1, k2, k3, k4,
			algebra::weighted_sum_5<double>( 1.0, 1/6.0, 1/3.0, 1/3.0, 1/6.0, h ) );

		return 0.0;
	}

private:

	void resize( usize_t n )
	{
		if ( kdata.size() != 3*n )
		{
			kdata.create( 3*n );

			k2.assign( &kdata[0*n], n );
			k3.assign( &kdata[1*n], n );
			k4.assign( &kdata[2*n], n );
		}
	}

	dr::vector<value_type>  kdata;
	dr::array <value_type>  k1, k2, k3, k4;
};

OSD_NS_END_
