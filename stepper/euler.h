
//==================================================
// @title        euler.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

//-------------------------------------------------------------
// Forward Euler method
//-------------------------------------------------------------

template <class _system>
struct Stepper_Euler
	: public Stepper<_system,1,false>
{
	typedef Stepper_Euler<_system>    self;
	typedef Stepper<_system,1,false>  parent;

	// Forward types
	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	double step( idata_type& idat, property_type& prop )
	{
		DBG_ASSERT_RVAL( idat.valid(), 0.0, "Invalid integration data.");

		idat.sys->derivative( idat.cur.t, idat.cur.x, idat.cur.dxdt );

		idat.next.t = idat.cur.t + idat.cur.dt;
		algebra::iter_apply_3( idat.next.x, idat.cur.x, idat.cur.dxdt,
			algebra::weighted_sum_2<double>( 1.0, idat.cur.dt ) );

		return 0.0;
	}
};

OSD_NS_END_
