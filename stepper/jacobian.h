
//==================================================
// @title        jacobian.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _system>
struct Stepper_Jacobian
	: public Stepper<_system,2,false,true>
{
	typedef Stepper_Jacobian<_system>  self;
	typedef Stepper<_system,2,false>   parent;

	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
    using idata_type     = typename parent::idata_type;

	// // Redefine timepoint and dependent types
	// using timepoint_type = TimePoint_Jacobian< value_type, time_type >;
	// using idata_type     = IntegrationData< system_type, timepoint_type >;
	// using property_type  = Properties< idata_type, no_metric >;

	// ----------  =====  ----------

	/**
	 * If I is the identity, and / is mrdivide:
	 *
	 * next.x = cur.x + ( cur.dxdt / cur.jac )*( expm(cur.dt*cur.jac) - I )
	 */

	double step( idata_type& d, property_type& prop )
	{
		DBG_ASSERT_RVAL( d.valid(), 0.0, "Invalid integration data.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;

		// Set current dxdt and jac
		sys.derivative ( d.cur.t, d.cur.x, d.cur.dxdt );
		sys.jacobian   ( d.cur.t, d.cur.x, d.cur.dfdx, d.cur.dfdt );

		// Export to Armadillo
		auto Jperm = d.cur.dfdx;
		Jperm.permute({1,0});

		auto Jtran = dr::dr2arma_mat( Jperm, true, false ); // deep copy of the transpose
		auto f     = dr::dr2arma_col( static_cast<dr::array<value_type>>(d.cur.dxdt), false, false );

		// dxdt / J = J' \ f = solve( J', f )
		f = arma::solve( Jtran, f );

		// dt * J
		d.cur.dfdx *= d.cur.dt;
		arma::mat J_dt   = dr::dr2arma_mat( d.cur.dfdx, false, false );
		arma::mat expJdt = arma::expmat( J_dt );

		// Set final state
		d.next.t = d.cur.t + d.cur.dt;
		for ( usize_t c = 0; c < dim; ++c )
		{
			d.next.x[c] = d.cur.x[c];
			for ( usize_t r = 0; r < dim; ++r )
				d.next.x[c] += f(r) * ( expJdt(r,c) - (r==c) );
		}

		return 0.0;
	}
};

OSD_NS_END_
