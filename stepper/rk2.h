
//==================================================
// @title        rk2.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

//-------------------------------------------------------------
// Runge-Kutta 2, aka midpoint method.
//-------------------------------------------------------------

template <class _system>
struct Stepper_RK2
	: public Stepper<_system,2,false>
{
	typedef Stepper_RK2<_system>      self;
	typedef Stepper<_system,2,false>  parent;

	// Forward types
	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	double step( idata_type& d, property_type& prop )
	{
		DBG_ASSERT_RVAL( d.valid(), 0.0, "Invalid integration data.");

		auto& sys = *d.sys;

		// Set cur.dxdt
		sys.derivative( d.cur.t, d.cur.x, d.cur.dxdt );

		// Half an Euler step
		algebra::iter_apply_3( d.next.x, d.cur.x, d.cur.dxdt,
			algebra::weighted_sum_2<double>( 1.0, d.cur.dt/2 ) );

		// Compute dxdt there
		d.next.t = d.cur.t + d.cur.dt/2;
		sys.derivative( d.next.t, d.next.x, d.next.dxdt );

		// Use this new estimate for an Euler step
		d.next.t = d.cur.t + d.cur.dt;
		algebra::iter_apply_3( d.next.x, d.cur.x, d.next.dxdt,
			algebra::weighted_sum_2<double>( 1.0, d.cur.dt ) );

		return 0.0;
	}

};

OSD_NS_END_
