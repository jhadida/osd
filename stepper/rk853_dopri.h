
//==================================================
// @title        rk853_dopri.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>

// Base on:
// http://www.unige.ch/~hairer/prog/nonstiff/dop853.f



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

// Constants for the Dormand-Prince 853 method.
struct rk853_dopri_constants {
	static const double
	t2, t3, t4, t5, t6, t7, t8, t9, t10, t11,
	a1, a4, a5, a6, a7, a8, a9, a10, a11,
	b1, b6, b7, b8, b9, b10, b11, b12,
	w21,
	w31, w32,
	w41, w43,
	w51, w53, w54,
	w61, w64, w65,
	w71, w74, w75, w76,
	w81, w84, w85, w86, w87,
	w91, w94, w95, w96, w97, w98,
	w101, w104, w105, w106, w107, w108, w109,
	w111, w114, w115, w116, w117, w118, w119, w1110,
	e11, e19, e112,
	e21, e26, e27, e28, e29, e210, e211, e212;
};

// ------------------------------------------------------------------------

template <class _system>
struct Stepper_RK853_Dopri
	: public Stepper<_system,8,true>, public rk853_dopri_constants
{
	typedef Stepper_RK853_Dopri<_system>  self;
	typedef Stepper<_system,8,true>       parent;

	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	void clear()
	{
		k1.clear();
		k2.clear();
		k3.clear();
		k4.clear();
		k5.clear();
		k6.clear();
		k7.clear();
		k8.clear();
		k9.clear();
		k10.clear();
		kdata.clear();

		yh.clear();
	}

	double step( idata_type& d, property_type& prop )
	{
		ASSERT_RVAL( d.valid(), 0.0, "Invalid state.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;


		// ----------  =====  ----------
		// Part 1: Evaluate K-points

		// Prepare k1 through k6 arrays
		resize(dim);
		k1 = d.cur.dxdt;

		auto h = d.cur.dt;
		auto x = d.next.x;

		// K1
		sys.derivative( d.cur.t, d.cur.x, k1 );

		// K2
		algebra::iter_apply_3( x, d.cur.x, k1,
			algebra::weighted_sum_2<double>( 1.0, h*w21 ) );
		sys.derivative( d.cur.t + h*t2, x, k2 );

		// K3
		algebra::iter_apply_4( x, d.cur.x, k1, k2,
			algebra::weighted_sum_3<double>( 1.0, w31, w32, h ) );
		sys.derivative( d.cur.t + h*t3, x, k3 );

		// K4
		algebra::iter_apply_4( x, d.cur.x, k1, k3,
			algebra::weighted_sum_3<double>( 1.0, w41, w43, h ) );
		sys.derivative( d.cur.t + h*t4, x, k4 );

		// K5
		algebra::iter_apply_5( x, d.cur.x, k1, k3, k4,
			algebra::weighted_sum_4<double>( 1.0, w51, w53, w54, h ) );
		sys.derivative( d.cur.t + h*t5, x, k5 );

		// K6
		algebra::iter_apply_5( x, d.cur.x, k1, k4, k5,
			algebra::weighted_sum_4<double>( 1.0, w61, w64, w65, h ) );
		sys.derivative( d.cur.t + h*t6, x, k6 );

		// K7
		algebra::iter_apply_6( x, d.cur.x, k1, k4, k5, k6,
			algebra::weighted_sum_5<double>( 1.0, w71, w74, w75, w76, h ) );
		sys.derivative( d.cur.t + h*t7, x, k7 );

		// K8
		algebra::iter_apply_7( x, d.cur.x, k1, k4, k5, k6, k7,
			algebra::weighted_sum_6<double>( 1.0, w81, w84, w85, w86, w87, h ) );
		sys.derivative( d.cur.t + h*t8, x, k8 );

		// K9
		algebra::iter_apply_8( x, d.cur.x, k1, k4, k5, k6, k7, k8,
			algebra::weighted_sum_7<double>( 1.0, w91, w94, w95, w96, w97, w98, h ) );
		sys.derivative( d.cur.t + h*t9, x, k9 );

		// K10
		algebra::iter_apply_9( x, d.cur.x, k1, k4, k5, k6, k7, k8, k9,
			algebra::weighted_sum_8<double>( 1.0, w101, w104, w105, w106, w107, w108, w109, h ) );
		sys.derivative( d.cur.t + h*t10, x, k10 );

		// K11
		algebra::iter_apply_10( x, d.cur.x, k1, k4, k5, k6, k7, k8, k9, k10,
			algebra::weighted_sum_9<double>( 1.0, w111, w114, w115, w116, w117, w118, w119, w1110, h ) );
		sys.derivative( d.cur.t + h*t11, x, k2 );


		// ----------  =====  ----------
		// Part 2: Final estimates
        d.next.t = d.cur.t + h;

		algebra::iter_apply_11( yh, d.cur.x, k1, k4, k5, k6, k7, k8, k9, k10, k2,
			algebra::weighted_sum_10<double>( 1.0, h*a1, h*a4, h*a5, h*a6, h*a7, h*a8, h*a9, h*a10, h*a11 ) );
		sys.derivative( d.cur.t + h, yh, k3 );

		// algebra::iter_apply_10( d.next.x, d.cur.x, k1, k6, k7, k8, k9, k10, k2, k3,
		// 	algebra::weighted_sum_9<double>( 1.0, h*b1, h*b6, h*b7, h*b8, h*b9, h*b10, h*b11, h*b12 ) );
		for ( usize_t i = 0; i < dim; ++i )
        {
            k4[i] = b1*k1[i] + b6*k6[i] + b7*k7[i] + b8*k8[i] + b9*k9[i] + b10*k10[i] + b11*k2[i] + b12*k3[i];
            d.next.x[i] = d.cur.x[i] + h*k4[i];
        }


		// ----------  =====  ----------
		// Part 3: Evaluate error

        double erri, err, err2, scale;
        err = err2 = 0.0;
        for ( usize_t i = 0; i < dim; ++i )
        {
            scale = std::max( std::abs(d.cur.x[i]), std::abs(d.next.x[i]) );
            scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
			// scale = prop.error.abs_tol + prop.error.rel_tol*scale;

            erri = (k4[i] - e11*k1[i] - e19*k9[i] - e112*k3[i]) / scale;
            err2 += erri*erri;

			erri = (e21*k1[i] + e26*k6[i] + e27*k7[i] + e28*k8[i] + e29*k9[i] + e210*k10[i] + e211*k2[i] + e212*k3[i]) / scale;
            err += erri*erri;
        }

		scale = err + 0.01 * err2;
		if ( scale <= 0.0 )
			scale = 1.0;

        return h * err * sqrt( 1.0/(dim*scale) );
		// return h * err * sqrt( 1.0/scale );
	}

private:

	void resize( usize_t n )
	{
		if ( yh.size() != n )
		{
			yh   .create(n);
			kdata.create(9*n);

			k2 .assign( &kdata[0*n], n );
			k3 .assign( &kdata[1*n], n );
			k4 .assign( &kdata[2*n], n );
			k5 .assign( &kdata[3*n], n );
			k6 .assign( &kdata[4*n], n );
			k7 .assign( &kdata[5*n], n );
			k8 .assign( &kdata[6*n], n );
			k9 .assign( &kdata[7*n], n );
			k10.assign( &kdata[8*n], n );
		}
	}

	dr::vector<value_type>  yh, kdata;
	dr::array <value_type>  k1, k2, k3, k4, k5, k6, k7, k8, k9, k10;
};

OSD_NS_END_
