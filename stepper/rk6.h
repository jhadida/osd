
//==================================================
// @title        rk6.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Based on:
// http://www.ams.org/journals/mcom/1968-22-102/S0025-5718-68-99876-1/S0025-5718-68-99876-1.pdf



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

struct rk6_constants {
	static const double
	a1, a3, a4, a5, a6, a7,
	t2, t3, t4, t5, t6,
	w21,
	w31, w32,
	w41, w42, w43,
	w51, w52, w53, w54,
	w61, w62, w63, w64, w65,
	w71, w72, w73, w74, w75, w76;
};

// ------------------------------------------------------------------------

template <class _system>
struct Stepper_RK6
	: public Stepper<_system,6,false>, public rk6_constants
{
	typedef Stepper_RK6<_system>      self;
	typedef Stepper<_system,6,false>  parent;

	// Forward types
	using system_type    = _system;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	void clear()
	{
		k1.clear();
		k2.clear();
		k3.clear();
		k4.clear();
		k5.clear();
		k6.clear();
		kdata.clear();
	}

	double step ( idata_type& d, property_type& prop )
	{
		ASSERT_RVAL( d.valid(), 0.0, "Invalid state.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;

		// Prepare k1 through k4 arrays
		resize(dim);
		k1 = d.cur.dxdt;

		auto h = d.cur.dt;
		auto x = d.next.x;

		// K1
		sys.derivative( d.cur.t, d.cur.x, k1 );

		// K2
		algebra::iter_apply_3( x, d.cur.x, k1,
			algebra::weighted_sum_2<double>( 1.0, h*w21 ) );
		sys.derivative( d.cur.t + h*t2, x, k2 );

		// K3
		algebra::iter_apply_4( x, d.cur.x, k1, k2,
			algebra::weighted_sum_3<double>( 1.0, w31, w32, h ) );
		sys.derivative( d.cur.t + h*t3, x, k3 );

		// K4
		algebra::iter_apply_5( x, d.cur.x, k1, k2, k3,
			algebra::weighted_sum_4<double>( 1.0, w41, w42, w43, h ) );
		sys.derivative( d.cur.t + h*t4, x, k4 );

		// K5
		algebra::iter_apply_6( x, d.cur.x, k1, k2, k3, k4,
			algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54, h ) );
		sys.derivative( d.cur.t + h*t5, x, k5 );

		// K6
		algebra::iter_apply_7( x, d.cur.x, k1, k2, k3, k4, k5,
			algebra::weighted_sum_6<double>( 1.0, w61, w62, w63, w64, w65, h ) );
		sys.derivative( d.cur.t + h*t6, x, k6 );

		// K7
		algebra::iter_apply_8( x, d.cur.x, k1, k2, k3, k4, k5, k6,
			algebra::weighted_sum_7<double>( 1.0, w71, w72, w73, w74, w75, w76, h ) );
		sys.derivative( d.cur.t + h, x, k2 );

		// Final estimate
		d.next.t = d.cur.t + h;
		algebra::iter_apply_7( d.next.x, d.cur.x, k1, k3, k5, k6, k2,
			algebra::weighted_sum_6<double>( 1.0, a1, a3, a5, a6, a7, h ) );

		return 0.0;
	}

private:

	void resize( usize_t n )
	{
		if ( kdata.size() != 5*n )
		{
			kdata.create( 5*n );

			k2.assign( &kdata[0*n], n );
			k3.assign( &kdata[1*n], n );
			k4.assign( &kdata[2*n], n );
			k5.assign( &kdata[3*n], n );
			k6.assign( &kdata[4*n], n );
		}
	}

	dr::vector<value_type>  kdata;
	dr::array <value_type>  k1, k2, k3, k4, k5, k6;
};

OSD_NS_END_
