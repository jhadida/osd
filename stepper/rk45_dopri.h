
//==================================================
// @title        rk45_dopri.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>

// Base on:
// http://www.unige.ch/~hairer/prog/nonstiff/dopri5.f



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

// Constants for the Dormand-Prince 45 method.
struct rk45_dopri_constants {
	static const double
	t2, t3, t4, t5, t6,
	a1, a3, a4, a5, a6,
	b1, b3, b4, b5, b6, b7,
	e1, e3, e4, e5, e6, e7,
	d1, d3, d4, d5, d6, d7,
	w21,
	w31, w32,
	w41, w42, w43,
	w51, w52, w53, w54,
	w61, w62, w63, w64, w65;
};

// ------------------------------------------------------------------------

template <class _system>
struct Stepper_RK45_Dopri
	: public Stepper<_system,5,true>, public rk45_dopri_constants
{
	typedef Stepper_RK45_Dopri<_system>  self;
	typedef Stepper<_system,5,true>      parent;

	using system_type    = _system;
	using metric_type    = typename parent::metric_type;
	using time_type      = typename parent::time_type;
	using value_type     = typename parent::value_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	void clear()
	{
		k1.clear();
		k2.clear();
		k3.clear();
		k4.clear();
		k5.clear();
		k6.clear();
		k7.clear();
		kdata.clear();
		yh.clear();
	}

	double step( idata_type& d, property_type& prop )
	{
		ASSERT_RVAL( d.valid(), 0.0, "Invalid state.");

		const usize_t dim = d.cur.size();
		system_type&  sys = *d.sys;


		// ----------  =====  ----------
		// Part 1: Evaluate K-points

		// Prepare k1 through k6 arrays
		resize(dim);
		k1 = d.cur.dxdt;
		k7 = d.next.dxdt;

		auto h = d.cur.dt;
		auto x = d.next.x;

		// K1
		sys.derivative( d.cur.t, d.cur.x, k1 );

		// K2
		algebra::iter_apply_3( x, d.cur.x, k1,
			algebra::weighted_sum_2<double>( 1.0, h*w21 ) );
		sys.derivative( d.cur.t + h*t2, x, k2 );

		// K3
		algebra::iter_apply_4( x, d.cur.x, k1, k2,
			algebra::weighted_sum_3<double>( 1.0, w31, w32, h ) );
		sys.derivative( d.cur.t + h*t3, x, k3 );

		// K4
		algebra::iter_apply_5( x, d.cur.x, k1, k2, k3,
			algebra::weighted_sum_4<double>( 1.0, w41, w42, w43, h ) );
		sys.derivative( d.cur.t + h*t4, x, k4 );

		// K5
		algebra::iter_apply_6( x, d.cur.x, k1, k2, k3, k4,
			algebra::weighted_sum_5<double>( 1.0, w51, w52, w53, w54, h ) );
		sys.derivative( d.cur.t + h*t5, x, k5 );

		// K6
		algebra::iter_apply_7( x, d.cur.x, k1, k2, k3, k4, k5,
			algebra::weighted_sum_6<double>( 1.0, w61, w62, w63, w64, w65, h ) );
		sys.derivative( d.cur.t + h, x, k6 );



		// ----------  =====  ----------
		// Part 2: Final estimates
		d.next.t = d.cur.t + h;

		algebra::iter_apply_7( d.next.x, d.cur.x, k1, k3, k4, k5, k6,
			algebra::weighted_sum_6<double>( 1.0, a1, a3, a4, a5, a6, h ) );
		sys.derivative( d.cur.t + h, d.next.x, k7 );

		// algebra::iter_apply_8( yh, d.cur.x, k1, k3, k4, k5, k6, k7,
		// 	algebra::weighted_sum_7<double>( 1.0, h*b1, h*b3, h*b4, h*b5, h*b6, h*b7 ) );
		algebra::iter_apply_7( k2, k1, k3, k4, k5, k6, k7,
			algebra::weighted_sum_6<double>( h*e1, e3, e4, e5, e6, e7, h ) );


		// ----------  =====  ----------
		// Part 3: Error estimate

		double error, scale, delta;
		if ( prop.error.norm_control )
		{
			scale = std::max( prop.metric.norm(d.cur.x), prop.metric.norm(d.next.x) );
			scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
			// scale = prop.error.abs_tol + prop.error.rel_tol * scale;
			error = prop.metric.norm(k2) / scale;
			// error = error / sqrt(dim);
		}
		else
		{
			error = 0.0;
			for ( usize_t i = 0; i < dim; ++i )
			{
				scale = std::max( std::abs(d.cur.x[i]), std::abs(d.next.x[i]) );
				scale = std::max( prop.error.abs_tol, prop.error.rel_tol * scale );
				// scale = prop.error.abs_tol + prop.error.rel_tol*scale;

				delta = k2[i]/scale;
				error += delta*delta;
			}
			error = sqrt( error/dim );
			// error = sqrt( error );
		}

		return error;
	}

private:

	void resize( usize_t n )
	{
		if ( yh.size() != n )
		{
			yh   .create(n);
			kdata.create(5*n);

			k2.assign( &kdata[0*n], n );
			k3.assign( &kdata[1*n], n );
			k4.assign( &kdata[2*n], n );
			k5.assign( &kdata[3*n], n );
			k6.assign( &kdata[4*n], n );
		}
	}

	dr::vector<value_type>  yh, kdata;
	dr::array <value_type>  k1, k2, k3, k4, k5, k6, k7;
};

OSD_NS_END_
