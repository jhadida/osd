
//==================================================
// @title        clamp.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Example plugin to enforce range-condition on states during integration.
 * Can be configured from an input Matlab structure.
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class IData>
struct Plugin_Clamp
	: public Plugin<IData>
{
public:

	using self        = Plugin_Clamp<IData>;
	using parent      = Plugin<IData>;
	using idata_type  = IData;
	using signal_type = typename parent::signal_type;
	using time_type   = typename IData::time_type;
	using value_type  = typename IData::value_type;

	// ----------  =====  ----------

	value_type lower, upper;

	Plugin_Clamp()
		{ configure(0,1); }
	Plugin_Clamp( signal_type& sig )
		: Plugin_Clamp()
		{ this->attach(sig); }

	bool configure( value_type l, value_type u )
	{
		lower = l; upper = u;
		return true;
	}


#ifdef OSD_USING_MATLAB

	bool configure( const dr::mx_struct& cfg )
	{
		ASSERT_RF( cfg, "[plugin.clamp] Invalid input." );
		ASSERT_RF( cfg.has_fields({"lower","upper"}), "[plugin.clamp] Missing field(s)." );

		ASSERT_RF( dr::mx_extract_scalar(cfg["lower"],lower),
			"[plugin.clamp] Could not extract 'lower'." );
		ASSERT_RF( dr::mx_extract_scalar(cfg["upper"],upper),
			"[plugin.clamp] Could not extract 'upper'." );

		return true;
	}

#endif

protected:

	virtual void callback( idata_type& dat )
	{
		for ( auto& x: dat.next.x )
			x = dr::op_clamp( x, lower, upper );
	}
};

OSD_NS_END_
