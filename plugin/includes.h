#ifndef OSD_PLUGIN_H_INCLUDED
#define OSD_PLUGIN_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "clamp.h"
#include "white_noise.h"

#endif