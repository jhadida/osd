
//==================================================
// @title        white_noise.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * Another example to add some noise on the states during integration.
 * White noises have zero-mean, but the std (sigma) is a parameter.
 */


		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class IData>
struct Plugin_WhiteNoise
	: public Plugin<IData>
{
public:

	using self        = Plugin_Clamp<IData>;
	using parent      = Plugin<IData>;
	using idata_type  = IData;
	using signal_type = typename parent::signal_type;
	using time_type   = typename IData::time_type;
	using value_type  = typename IData::value_type;

	// ----------  =====  ----------
	
	// Noise-related stuff
	typedef std::normal_distribution<value_type>  noise_dist;
	typedef dr::generator_type<noise_dist>        noise_gen;

	noise_gen noise; // the noise generator

	Plugin_WhiteNoise()
		{ configure(1); }
	Plugin_WhiteNoise( signal_type& sig )
		: Plugin_WhiteNoise()
		{ this->attach(sig); }

	bool configure( value_type sigma )
	{
		noise = dr::normal_gen<value_type>( 0, sigma );
		return true;
	}


#ifdef OSD_USING_MATLAB

	bool configure( const dr::mx_struct& cfg )
	{
		ASSERT_RF( cfg, "[plugin.white_noise] Invalid input." );
		ASSERT_RF( cfg.has_field("sigma"), "[plugin.white_noise] Missing field 'sigma'." );

		value_type sigma;
		ASSERT_RF( dr::mx_extract_scalar(cfg["sigma"],sigma),
			"[plugin.white_noise] Could not extract 'sigma'." );

		return configure(sigma);
	}

#endif

protected:

	void callback( idata_type& dat )
	{
		for ( auto& x: dat.next.x )
			x = x + noise();
	}
};

OSD_NS_END_
