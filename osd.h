#ifndef OSD_H_INCLUDED
#define OSD_H_INCLUDED

//==================================================
// @title        osd.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#define OSD_NS_START_ namespace osd {
#define OSD_NS_END_   }

// ----------  =====  ----------

#define OSD_NUMJAC_FORWARD 1
#define OSD_NUMJAC_CENTRAL 2

#ifndef OSD_BLOCK_SIZE
#define OSD_BLOCK_SIZE 1000
#endif
#ifndef OSD_NUMJAC_METHOD
#define OSD_NUMJAC_METHOD OSD_NUMJAC_FORWARD
#endif
#ifndef OSD_MATLAB_INTERRUPT_N
#define OSD_MATLAB_INTERRUPT_N 1000
#endif

// ------------------------------------------------------------------------

// The Jacobian methods require Armadillo (expm)
#ifndef DRAYN_USING_ARMADILLO
#define DRAYN_USING_ARMADILLO
#endif

// Drayn must be included in the path
#include "drayn.h"

// Propagate Matlab flag from Drayn
#ifdef DRAYN_USING_MATLAB

	#ifndef OSD_USING_MATLAB
	#define OSD_USING_MATLAB
	#endif

#endif

// ------------------------------------------------------------------------

#include "traits.h"

#include "utils/includes.h"
#include "core/includes.h"
#include "stepper/includes.h"
#include "integrator/includes.h"
#include "plugin/includes.h"

#endif
