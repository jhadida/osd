
//==================================================
// @title        traits.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

// Source integer types from Drayn
using namespace dr::int_types;

// ------------------------------------------------------------------------

template <class T> using ref_vector  = dr::array  <       T >;
template <class T> using cref_vector = dr::array  < const T >;

template <class T> using ref_matrix  = dr::matrix<       T , dr::array<      T> >;
template <class T> using cref_matrix = dr::matrix< const T , dr::array<const T> >;

template <class value_type, class time_type>
struct tp_traits
{
	using state_type    = ref_vector<value_type>;
	using deriv_type    = dr::vector<value_type>;
	using jacobian_type = dr::matrix<value_type>;
};

// ------------------------------------------------------------------------

template <class _value>
using default_metric = dr::EuclideanMetric< ref_vector<_value> >;

struct no_metric {};

OSD_NS_END_
