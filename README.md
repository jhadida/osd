[![License: MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)

# Differential Equations Solvers using Drayn

OSD is a C++ library built on top of [Drayn](https://github.com/Sheljohn/drayn) to solve systems of differential equations.
The design resembles that of Boost's [`odeint`](http://headmyshoulder.github.io/odeint-v2/) library, but with added constrains on the state-type and memory allocation, and using a pub-sub pattern to provide hooks during integration.
There are several solvers currently implemented; explicit and implicit, with fixed or adaptive time-steps. Suggestions for improvement or future developments are most welcome (please open [an issue](https://github.com/Sheljohn/osd/issues)).

If you are fluent in C++, you can read about the design in the `core/` directory.
Otherwise please come back later for the documentation; it is being written slowly in [the wiki](https://github.com/Sheljohn/osd/wiki).
