
//==================================================
// @title        info.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <string>



		/********************     **********     ********************/
		/********************     **********     ********************/


// Not in namespace
// OSD_NS_START_

struct osd_info_private_
{
	bool flag;
	osd_info_private_(): flag(true) {}
};

inline bool osd_info_status()
	{ return dr::static_val<osd_info_private_>().flag; }
inline void osd_info_enable()
	{ dr::static_val<osd_info_private_>().flag = true; }
inline void osd_info_disable()
	{ dr::static_val<osd_info_private_>().flag = false; }

// ------------------------------------------------------------------------

template <class... Args>
void osd_info( std::string fmt, Args&&... args )
{
	if ( osd_info_status() )
	{
		fmt += "\n"; dr_print( fmt.c_str(), std::forward<Args>(args)... );
	}
}

// OSD_NS_END_
