
//==================================================
// @title        iter_apply.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

namespace algebra {

	template <class S1, class Op>
	inline void iter_apply_1( const S1& s1, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i] );
	}
	
	template <class S1, class S2, class Op>
	inline void iter_apply_2( const S1& s1, const S2& s2, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i] );
	}

	template <class S1, class S2, class S3, class Op>
	inline void iter_apply_3( const S1& s1, const S2& s2, const S3& s3, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i] );
	}

	template <class S1, class S2, class S3, class S4, class Op>
	inline void iter_apply_4( const S1& s1, const S2& s2, const S3& s3, const S4& s4, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class Op>
	inline void iter_apply_5( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class Op>
	inline void iter_apply_6( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class Op>
	inline void iter_apply_7( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class Op>
	inline void iter_apply_8( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class Op>
	inline void iter_apply_9( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class Op>
	inline void iter_apply_10( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class S11, class Op>
	inline void iter_apply_11( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, const S11& s11, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i], s11[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class S11, class S12, class Op>
	inline void iter_apply_12( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, const S11& s11, const S12& s12, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i], s11[i], s12[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class S11, class S12, class S13, class Op>
	inline void iter_apply_13( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, const S11& s11, const S12& s12, const S13& s13, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i], s11[i], s12[i], s13[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class S11, class S12, class S13, class S14, class Op>
	inline void iter_apply_14( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, const S11& s11, const S12& s12, const S13& s13, const S14& s14, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i], s11[i], s12[i], s13[i], s14[i] );
	}

	template <class S1, class S2, class S3, class S4, class S5, class S6, class S7, class S8, class S9, class S10, class S11, class S12, class S13, class S14, class S15, class Op>
	inline void iter_apply_15( const S1& s1, const S2& s2, const S3& s3, const S4& s4, const S5& s5, const S6& s6, const S7& s7, const S8& s8, const S9& s9, const S10& s10, const S11& s11, const S12& s12, const S13& s13, const S14& s14, const S15& s15, Op op )
	{
		const usize_t d = s1.size();
		for ( usize_t i = 0; i < d; ++i )
			op( s1[i], s2[i], s3[i], s4[i], s5[i], s6[i], s7[i], s8[i], s9[i], s10[i], s11[i], s12[i], s13[i], s14[i], s15[i] );
	}
}

OSD_NS_END_
