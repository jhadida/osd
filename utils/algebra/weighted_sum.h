
//==================================================
// @title        weighted_sum.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

namespace algebra {

	template <class W = double>
	struct weighted_sum_1
	{
		const W m_w1;

		weighted_sum_1( const W& w1 )
			: m_w1(w1)
			{}

		template <class Tout, class T1>
		void operator() ( Tout& out, const T1& t1 ) const
		{
			out = static_cast<Tout>( m_w1*t1 );
		}
	};

	template <class W = double>
	struct weighted_sum_2
	{
		const W m_w1, m_w2;

		weighted_sum_2( const W& w1, const W& w2 )
			: m_w1(w1), m_w2(w2)
			{}

		template <class Tout, class T1, class T2>
		void operator() ( Tout& out, const T1& t1, const T2& t2 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_w2*t2 );
		}
	};

	template <class W = double>
	struct weighted_sum_3
	{
		const W m_h;
		const W m_w1, m_w2, m_w3;

		weighted_sum_3( const W& w1, const W& w2, const W& w3, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3) );
		}
	};

	template <class W = double>
	struct weighted_sum_4
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4;

		weighted_sum_4( const W& w1, const W& w2, const W& w3, const W& w4, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4) );
		}
	};

	template <class W = double>
	struct weighted_sum_5
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5;

		weighted_sum_5( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5) );
		}
	};

	template <class W = double>
	struct weighted_sum_6
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6;

		weighted_sum_6( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& w6, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6) );
		}
	};

	template <class W = double>
	struct weighted_sum_7
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7;

		weighted_sum_7( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& w6, const W& w7, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6, const T7& t7 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7) );
		}
	};

	template <class W = double>
	struct weighted_sum_8
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8;

		weighted_sum_8( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& w6, const W& w7, const W& w8, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6, const T7& t7, const T8& t8 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8) );
		}
	};

	template <class W = double>
	struct weighted_sum_9
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8, m_w9;

		weighted_sum_9( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& w6, const W& w7, const W& w8, const W& w9, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_w9(w9), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6, const T7& t7, const T8& t8, const T9& t9 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8 + m_w9*t9) );
		}
	};

	template <class W = double>
	struct weighted_sum_10
	{
		const W m_h;
		const W m_w1, m_w2, m_w3, m_w4, m_w5, m_w6, m_w7, m_w8, m_w9, m_w10;

		weighted_sum_10( const W& w1, const W& w2, const W& w3, const W& w4, const W& w5, const W& w6, const W& w7, const W& w8, const W& w9, const W& w10, const W& h=1.0 )
			: m_w1(w1), m_w2(w2), m_w3(w3), m_w4(w4), m_w5(w5), m_w6(w6), m_w7(w7), m_w8(w8), m_w9(w9), m_w10(w10), m_h(h)
			{}

		template <class Tout, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
		void operator() ( Tout& out, const T1& t1, const T2& t2, const T3& t3, const T4& t4, const T5& t5, const T6& t6, const T7& t7, const T8& t8, const T9& t9, const T10& t10 ) const
		{
			out = static_cast<Tout>( m_w1*t1 + m_h*(m_w2*t2 + m_w3*t3 + m_w4*t4 + m_w5*t5 + m_w6*t6 + m_w7*t7 + m_w8*t8 + m_w9*t9 + m_w10*t10) );
		}
	};

}

OSD_NS_END_
