
//==================================================
// @title        lu.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>
#include <type_traits>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class T>
struct LUDecomposition
{
    static_assert( std::is_floating_point<T>::value, "Floating-point type required." );

    using Integer = int64_t;
    using Number  = T;
    using Matrix  = arma::Mat<Number>;
    using Vector  = arma::Col<Number>;
    using Indices = arma::Col<Integer>;
    using DrArray = dr::array<Number>;

    Integer n;
    Matrix lu;
    Indices idx;

    LUDecomposition(): n(0) {}

    explicit LUDecomposition( Integer size )
        { resize(size); }
    explicit LUDecomposition( const Matrix& a )
        { assign(a); decompose(); }

    void resize( Integer size );
    void assign( const Matrix& a );

    void decompose();
    void solve( const DrArray& b, const DrArray& x ) const;

    // void solve( const Vector& b, Vector& x );
    // void solve( const Matrix& b, Matrix& x );

private:
    mutable Vector tvec; // temporary vec
};

// ------------------------------------------------------------------------

template <class T>
void LUDecomposition<T>::resize( Integer size )
{
    n = size;
    lu = Matrix(n,n);
    idx = Indices(n);
    tvec = Vector(n);
}

template <class T>
void LUDecomposition<T>::assign( const Matrix& a )
{
    n = a.n_rows;
    lu = a;
    idx = Indices(n);
    tvec = Vector(n);
}

template <class T>
void LUDecomposition<T>::decompose()
{
    static const Number ONE  = Number(1);
    static const Number EPS  = dr::c_num<Number>::eps;
    static const Number ZERO = Number(0);

    Integer i, j, k, imax;
    Number big, tmp;

    for (i=0;i<n;i++)
    {
        // find absolute largest element in row
        big=ZERO;
        for (j=0;j<n;j++)
            if ( (tmp=std::abs(lu(i,j))) > big )
                big=tmp;

        // store its inverse
        if (big < EPS)
            throw std::runtime_error("Singular matrix in LUDecomposition");
        tvec(i)=ONE/big;
    }

    for (k=0;k<n;k++)
    {
        big=ZERO;
        for (i=k;i<n;i++)
        {
            tmp=tvec(i)*std::abs(lu(i,k));
            if (tmp > big)
            {
                big=tmp;
                imax=i;
            }
        }

        if (k != imax)
        {
            for (j=0;j<n;j++)
            {
                tmp=lu(imax,j);
                lu(imax,j)=lu(k,j);
                lu(k,j)=tmp;
            }
            tvec(imax)=tvec(k);
        }

        idx(k)=imax;
        if ( lu(k,k) >= ZERO )
            lu(k,k) = std::max( lu(k,k), EPS );
        else
            lu(k,k) = std::min( lu(k,k), -EPS );

        for (i=k+1;i<n;i++)
        {
            tmp=(lu(i,k) /= lu(k,k));
            for (j=k+1;j<n;j++)
                lu(i,j) -= tmp*lu(k,j);
        }
    }
}

template <class T>
void LUDecomposition<T>::solve( const DrArray& b, const DrArray& x ) const
{
    static const Number ZERO = Number(0);

    Integer i, j, k=0, ip;
    Number sum;

    if ( b.size() != n || x.size() != n )
        throw std::runtime_error("LUDecomposition::solve bad sizes");

    for (i=0;i<n;i++)
        x[i] = b[i];

    for (i=0;i<n;i++)
    {
        ip=idx(i);
        sum=x[ip];
        x[ip]=x[i];

        if (k != 0)
            for (j=k-1;j<i;j++)
                sum -= lu(i,j)*x[j];
        else if (sum != ZERO)
            k=i+1;

        x[i]=sum;
    }

    for (i=n-1;i>=0;i--)
    {
        sum=x[i];
        for (j=i+1;j<n;j++)
            sum -= lu(i,j)*x[j];

        x[i]=sum/lu(i,i);
    }
}

OSD_NS_END_

// template <class T>
// void LUDecomposition<T>::solve( const Matrix& b, Matrix& x )
// {
//     if ( b.n_rows != n || x.n_rows != n || b.n_cols != x.n_cols )
//         throw std::runtime_error("LUDecomposition::solve bad sizes");
//
//     Integer m = b.n_cols;
//     for ( Integer i=0; i<m; i++ )
//         solve( b.col(i), x.col(i) );
// }
