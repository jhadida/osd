
//==================================================
// @title        timecourse.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time, bool _managed = true>
class TimeCourse_base
{
public:

    typedef _value  value_type;
    typedef _time   time_type;

    using vec_time = dr::array<time_type>;
    using vec_vals = dr::array<value_type>;
    using vec_slop = typename std::conditional< _managed, dr::vector<double>, dr::array<double> >::type;

	// ----------  =====  ----------

	TimeCourse_base()
		{ clear(); }

	void clear()
    {
        m_time .clear();
        m_vals .clear();
        m_slop .clear();

        m_arithmetic = false;
    }

    // validity
    inline bool valid() const
    {
        return m_time.valid() && m_vals.valid() && (m_vals.size()==m_time.size());
        // m_slop.valid() && (m_slop.size()==m_time.size())
    }
    inline bool empty() const { return n_times() == 0; }
    inline operator bool() const { return valid() && !empty(); }

    // dimensions
    inline usize_t n_times    () const { return m_time.size(); }
    inline usize_t n_signals  () const { return 1; }
    inline usize_t n_channels () const { return n_signals(); }
    inline usize_t state_size () const { return n_signals(); }

    // references
    inline const vec_time& time () const { return m_time; }
    inline const vec_vals& vals () const { return m_vals; }

    // access
    inline time_type&  time  ( usize_t t ) const { return m_time[t]; }
    inline value_type& value ( usize_t t ) const { return m_vals[t]; }
    inline double&     slope ( usize_t t ) const { return m_slop[t]; }

    // time-info
    inline time_type tspan() const
        { return n_times() ? (m_time.back()-m_time.front()) : time_type(0); }

    // operator[] gives the corresponding TIME
    inline time_type& operator[] ( usize_t t ) const { return time(t); }

    // operator() gives the corresponding VALUE (time and signal index)
    inline value_type& operator() ( usize_t t, usize_t k=0 ) const { return value(t); }

    // interpolation
    inline double ninterp( time_type t, usize_t k=0 ) const
        { return nearest_value_tc( m_time, m_vals, n_times(), t, m_arithmetic ); }
    inline double linterp( time_type t, usize_t k=0 ) const
        { return interp_linear_value_tc( m_time, m_vals, n_times(), t, m_arithmetic ); }
    inline double pinterp( time_type t, usize_t k=0 ) const
        { init_slopes(); return interp_pchip_value_tc( m_time, m_vals, m_slop, n_times(), t, m_arithmetic ); }

protected:

    virtual void init_slopes() const =0;

    bool m_arithmetic;
    mutable vec_slop m_slop;

	vec_time  m_time;
	vec_vals  m_vals;
};



//--------------------     ==========     --------------------//
//--------------------     **********     --------------------//



template <class _value, class _time>
class TimeCourse
    : public TimeCourse_base<_value,_time,true>
{
public:

    typedef _value  value_type;
    typedef _time   time_type;

    using parent   = TimeCourse_base<_value,_time,true>;
    using vec_time = typename parent::vec_time;
    using vec_vals = typename parent::vec_vals;
    using vec_slop = typename parent::vec_slop;

    // ----------  =====  ----------

    TimeCourse() {}
    TimeCourse( const vec_time& t, const vec_vals& v )
        { assign( t, v ); }

    bool assign( const vec_time& t, const vec_vals v )
    {
        this->clear();
        const usize_t nt = t.size();

        ASSERT_RF( nt > 0, "Input is empty." );
        ASSERT_RF( nt == v.size(), "Input size mismatch." );

        this->m_time = t;
        this->m_vals = v;

        // check if the timecourse is arithmetic
        if ( nt > 1 )
        {
            this->m_arithmetic = true;
            time_type h = t[1]-t[0];

            for ( usize_t i = 2; i < nt; ++i )
                this->m_arithmetic = this->m_arithmetic && (std::abs(t[i]-t[i-1]-h) < 1e-12);
        }

        INFO_ASSERT( this->m_arithmetic,
            "[osd.TimeCourse] Input timecourse is not arithmetically sampled, "
            "this will affect runtime quite badly if you use interpolation."
        );

        return true;
    }

protected:

    void init_slopes() const
    {
        usize_t nt = this->m_time.size();
        if ( this->m_slop.size() != nt )
        {
            // compute slopes for interpolation
            this->m_slop.create(nt);
            for ( usize_t i = 0; i < nt; ++i )
                set_pchip_slopes_tc( this->m_time, this->m_vals, nt, this->m_slop, i );
        }
    }
};

// ------------------------------------------------------------------------

template <class _value, class _time>
class TimeCourseReference
    : public TimeCourse_base<_value,_time,false>
{
public:

    typedef _value  value_type;
    typedef _time   time_type;

    using parent   = TimeCourse_base<_value,_time,false>;
    using vec_time = typename parent::vec_time;
    using vec_vals = typename parent::vec_vals;
    using vec_slop = typename parent::vec_slop;

    // ----------  =====  ----------

    TimeCourseReference() {}
    TimeCourseReference( const vec_time& t, const vec_vals& v, const vec_slop& d )
        { assign( t, v, d ); }

    bool assign( const vec_time& t, const vec_vals v, const vec_slop& d )
    {
        this->clear();
        const usize_t nt = t.size();

        ASSERT_RF( nt > 0, "Input is empty." );
        ASSERT_RF( (nt == v.size()) && (nt == d.size()), "Input size mismatch." );

        this->m_time = t;
        this->m_vals = v;
        this->m_slop = d;

        // check if the timecourse is arithmetic
        if ( nt > 1 )
        {
            this->m_arithmetic = true;
            time_type h = t[1]-t[0];

            for ( usize_t i = 2; i < nt; ++i )
                this->m_arithmetic = this->m_arithmetic && (std::abs(t[i]-t[i-1]-h) < 1e-12);
        }

        INFO_ASSERT( this->m_arithmetic,
            "[osd.TimeCourseReference] Input timecourse is not arithmetically sampled, "
            "this will affect runtime quite badly if you use interpolation."
        );

        return true;
    }

protected:

    void init_slopes() const {} // nothing to do
};

OSD_NS_END_
