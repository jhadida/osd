
//==================================================
// @title        timeseries.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time>
class TimeSeries
{
public:

    typedef TimeSeries<_value,_time>  self;
    typedef _value                    value_type;
    typedef _time                     time_type;

    using vec_time = dr::array< time_type >;
    using mat_vals = dr::matrix< value_type, dr::array<value_type> >;
    using mat_slop = dr::matrix< double >;
    using tc_type  = TimeCourseReference< _value, _time >;

    typedef typename mat_vals::col_type  col_type;
    typedef typename mat_vals::row_type  row_type;

	// ----------  =====  ----------

	TimeSeries()
		{ clear(); }
    TimeSeries( const vec_time& t, const mat_vals& v )
        { ASSERT_THROW( assign(t,v), "Could not build TimeSeries object." ); }
    TimeSeries( const self& that )
        { operator=(that); }

	void clear();

    // assignment
	bool assign( const vec_time& t, const mat_vals& v );

    // validity
    virtual bool valid() const
        { return m_time.valid() && m_vals.valid() && (m_time.size() == m_vals.nrows()); }
    inline bool empty() const { return n_times() == 0; }
    inline operator bool() const { return this->valid() && !empty(); }

    // dimensions
    inline usize_t n_times    () const { return m_time.size(); }
    inline usize_t n_signals  () const { return m_vals.ncols(); }
    inline usize_t size       () const { return n_times(); }
    inline usize_t n_channels () const { return n_signals(); }
    inline usize_t state_size () const { return n_signals(); }

    // time-step
    inline bool is_arithmetic() const { return m_arithmetic; }

    // references
    inline const vec_time& time () const { return m_time; }
    inline const mat_vals& vals () const { return m_vals; }

    // access
    inline time_type&  time  ( usize_t t ) const { return m_time[t]; }
    inline value_type& value ( usize_t t, usize_t k ) const { return m_vals(t,k); }
    inline double&     slope ( usize_t t, usize_t k ) const { return m_slop(t,k); }

    inline col_type signal ( usize_t k ) const { return m_vals.col(k); }
    inline row_type state  ( usize_t t ) const { return m_vals.row(t); }

    // time-info
    inline time_type tspan() const
        { return n_times() ? (m_time.back()-m_time.front()) : time_type(0); }

    // operator[] gives the corresponding TIME
    inline time_type& operator[] ( usize_t t ) const { return time(t); }

    // operator() gives the corresponding VALUE (time and signal index)
    inline value_type& operator() ( usize_t t, usize_t k ) const { return value(t,k); }

    // assignment of another time-series (shallow copy)
    self& operator= ( const self& that );

    // interpolation
    inline double ninterp( time_type t, usize_t k ) const
        { return nearest_value_ts( m_time, m_vals, n_times(), t, k, m_arithmetic ); }
    inline double linterp( time_type t, usize_t k ) const
        { return interp_linear_value_ts( m_time, m_vals, n_times(), t, k, m_arithmetic ); }
    inline double pinterp( time_type t, usize_t k ) const
        { init_slopes(); return interp_pchip_value_ts( m_time, m_vals, m_slop, n_times(), t, k, m_arithmetic ); }

    template <class S>
    inline void ninterps( time_type t, const S& state ) const
        { nearest_state_ts( m_time, m_vals, n_times(), t, state, state.size(), m_arithmetic ); }
    template <class S>
    inline void linterps( time_type t, const S& state ) const
        { interp_linear_state_ts( m_time, m_vals, n_times(), t, state, state.size(), m_arithmetic ); }
    template <class S>
    inline void pinterps( time_type t, const S& state ) const
        { init_slopes(); interp_pchip_state_ts( m_time, m_vals, m_slop, n_times(), t, state, state.size(), m_arithmetic ); }

    // get a particular timecourse
    inline tc_type timecourse( usize_t k ) const
        { return tc_type( m_time, m_vals.col(k).to_array(), m_slop.col(k).to_array() ); }

protected:

    void init_slopes() const
    {
        if ( m_slop.numel() != m_vals.numel() || m_slop.ncols() != m_vals.ncols() )
        {
            m_slop = dr::create_matrix<double>( m_vals.nrows(), m_vals.ncols(), dr::MemoryLayout::Columns );
            for ( usize_t i = 0; i < m_time.size(); ++i )
                set_pchip_slopes_ts( m_time, m_vals, m_vals.nrows(), m_vals.ncols(), m_slop, i );
        }
    }

    bool m_arithmetic;
    mutable mat_slop m_slop;

	vec_time  m_time;
	mat_vals  m_vals;
};

// ------------------------------------------------------------------------

template <class V, class T>
void TimeSeries<V,T>::clear()
{
    m_time .clear();
    m_vals .clear();
    m_slop .clear();
    m_arithmetic = false;
}

// ------------------------------------------------------------------------

template <class V, class T>
auto TimeSeries<V,T>::operator= ( const self& that ) -> self&
{
    m_time = that.m_time;
    m_vals = that.m_vals;
    m_slop = that.m_slop;
    m_arithmetic = that.m_arithmetic;

    return *this;
}

// ------------------------------------------------------------------------

template <class V, class T>
bool TimeSeries<V,T>::assign( const vec_time& t, const mat_vals& v )
{
    clear();
    const usize_t nt = t.size();

    ASSERT_RF( (nt > 0) && (v.ncols() > 0),
        "[osd.TimeSeries] One or more input is empty." )
    ASSERT_RF( (nt == v.nrows()),
        "[osd.TimeSeries] The number of rows must match the number of timepoints." );

    m_time = t;
    m_vals = v;

    // check if the timecourse is arithmetic
    if ( nt > 1 )
    {
        m_arithmetic = true;
        time_type h  = t[1]-t[0];

        for ( usize_t i = 2; i < nt; ++i )
            m_arithmetic = m_arithmetic && (std::abs(t[i]-t[i-1]-h) < 1e-12);
    }

    INFO_ASSERT( m_arithmetic,
        "[osd.TimeSeries] Input time-series is not arithmetically sampled, "
        "this will affect runtime quite badly if you use interpolation."
    );

    return true;
}

OSD_NS_END_
