
//==================================================
// @title        interp_pchip.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

// Piecewise Cubic Hermite Interpolating Polynomial (PCHIP)

#include <iostream>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * For all inner points:
 *
 * d(k) = pchip_slope( t(k)-t(k-1), t(k+1)-t(k), v(k)-v(k-1), v(k+1)-v(k) );
 */
template <class T, class V>
V pchip_slope( const T& h1, const T& h2, const V& D1, const V& D2 )
{
	static const V zero = V(0);
	static const V eps  = dr::c_num<V>::eps;

	if ( (std::abs(D1) < eps) || (std::abs(D2) < eps) || (D1*D2 < zero) )
		return zero;

	T w1 = 2*h2 + h1;
	T w2 = h2 + 2*h1;

	return static_cast<V>( (w1+w2) / ( h1*w1/D1 + h2*w2/D2 ) );
}

/**
 * For the first and last point:
 *
 * d(0) = pchip_slope_end( t(1)-t(0), t(2)-t(1), v(1)-v(0), v(2)-v(1) )
 * d(n-1) = pchip_slope_end( t(n-1)-t(n-2), t(n-2)-t(n-3), v(n-1)-v(n-2), v(n-2)-v(n-3) )
 */
template <class T, class V>
V pchip_slope_end( const T& h1, const T& h2, const V& D1, const V& D2 )
{
	static const V zero = V(0);

	V d = ( (2*h1+h2)*D1/h1 - h1*D2/h2 ) / (h1+h2);
	if ( d*D1 < zero )
		return zero;
	else if ( (D1*D2 < zero) && (std::abs(d) > std::abs(3*D1/h1)) )
		return 3*D1/h1;
	else
		return d;
}

// ------------------------------------------------------------------------

template <class V, class D>
void set_pchip_slopes( const V& values, usize_t nt, usize_t ns, D& slopes, usize_t t )
{
	ASSERT_R( nt > 2, "Not enough timepoints." );
	ASSERT_R( t < nt, "Bad timepoint index." );
	if ( t == 0 )
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope_end(
				values[1]-values[0], values[2]-values[1],
				values(1,i)-values(0,i), values(2,i)-values(1,i)
			);
	}
	else if ( t == (nt-1) )
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope_end(
				values[nt-1]-values[nt-2], values[nt-2]-values[nt-3],
				values(nt-1,i)-values(nt-2,i), values(nt-2,i)-values(nt-3,i)
			);
	}
	else
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope(
				values[t]-values[t-1], values[t+1]-values[t],
				values(t,i)-values(t-1,i), values(t+1,i)-values(t,i)
			);
	}
}

template <class T, class V, class D>
void set_pchip_slopes_ts( const T& time, const V& values, usize_t nt, usize_t ns, D& slopes, usize_t t )
{
	ASSERT_R( nt > 2, "Not enough timepoints." );
	ASSERT_R( t < nt, "Bad timepoint index." );
	if ( t == 0 )
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope_end(
				time[1]-time[0], time[2]-time[1],
				values(1,i)-values(0,i), values(2,i)-values(1,i)
			);
	}
	else if ( t == (nt-1) )
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope_end(
				time[nt-1]-time[nt-2], time[nt-2]-time[nt-3],
				values(nt-1,i)-values(nt-2,i), values(nt-2,i)-values(nt-3,i)
			);
	}
	else
	{
		for ( usize_t i = 0; i < ns; ++i )
			slopes(t,i) = pchip_slope(
				time[t]-time[t-1], time[t+1]-time[t],
				values(t,i)-values(t-1,i), values(t+1,i)-values(t,i)
			);
	}
}

template <class T, class V, class D>
void set_pchip_slopes_tc( const T& time, const V& values, usize_t nt, D& slopes, usize_t t )
{
	ASSERT_R( nt > 2, "Not enough timepoints." );
	ASSERT_R( t < nt, "Bad timepoint index." );
	if ( t == 0 )
	{
		slopes[t] = pchip_slope_end(
			time[1]-time[0], time[2]-time[1],
			values[1]-values[0], values[2]-values[1]
		);
	}
	else if ( t == (nt-1) )
	{
		slopes[t] = pchip_slope_end(
			time[nt-1]-time[nt-2], time[nt-2]-time[nt-3],
			values[nt-1]-values[nt-2], values[nt-2]-values[nt-3]
		);
	}
	else
	{
		slopes[t] = pchip_slope(
			time[t]-time[t-1], time[t+1]-time[t],
			values[t]-values[t-1], values[t+1]-values[t]
		);
	}
}

// ------------------------------------------------------------------------

template <class T, class V, class D>
double interp_pchip_ts( const T& time, const V& values, const D& slopes, usize_t nt, usize_t index, const LinearInterpolant& lin )
{
	static const double eps = dr::c_num<double>::eps;
	double s, b, c;
	double vl, vr, dl, dr, h;

	switch ( nt )
	{
		case 0:
			return 0.0;
		case 1:
			return values( lin.left, index );
		case 2:
			return
				lin.wleft  * values( lin.left,  index ) +
				lin.wright * values( lin.right, index ) ;
		default:

			dl = slopes( lin.left,  index );
			dr = slopes( lin.right, index );
			vl = values( lin.left,  index );
			vr = values( lin.right, index );
			h  = time[lin.right] - time[lin.left];

			if ( h*h > eps )
			{
				c  = ( 3*(vr-vl)/h - 2*dl - dr ) / h;
				b  = ( dl - 2*(vr-vl)/h + dr ) / (h*h);
				s  = h * lin.wright;
				return vl + s*( dl + s*(c + s*b) );
			}
			else return (vl+vr)/2.0;
	}
}

template <class V, class D>
inline double interp_pchip( const V& values, const D& slopes, usize_t nt, usize_t index, const LinearInterpolant& lin )
	{ return interp_pchip_ts( values, values, slopes, nt, index, lin ); }

template <class T, class V, class D>
double interp_pchip_tc( const T& time, const V& values, const D& slopes, usize_t nt, const LinearInterpolant& lin )
{
	static const double eps = dr::c_num<double>::eps;
	double s, b, c;
	double vl, vr, dl, dr, h;

	switch ( nt )
	{
		case 0:
			return 0.0;
		case 1:
			return values[ lin.left ];
		case 2:
			return
				lin.wleft  * values[ lin.left  ] +
				lin.wright * values[ lin.right ] ;
		default:

			dl = slopes[ lin.left  ];
			dr = slopes[ lin.right ];
			vl = values[ lin.left  ];
			vr = values[ lin.right ];
			h  = time[lin.right] - time[lin.left];

			if ( h*h > eps )
			{
				c  = ( 3*(vr-vl)/h - 2*dl - dr ) / h;
				b  = ( dl - 2*(vr-vl)/h + dr ) / (h*h);
				s  = h * lin.wright;
				return vl + s*( dl + s*(c + s*b) );
			}
			else return (vl+vr)/2.0;
	}
}

// ------------------------------------------------------------------------

template <class V, class D, class T, class S>
void interp_pchip_state( const V& values, const D& slopes, usize_t nt, const T& query, const S& state, usize_t ssize, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( values, nt, query, interpolant );
	else
		interp_linear_ascending( values, nt, query, interpolant );

	for ( usize_t i = 0; i < ssize; ++i )
		state[i] = interp_pchip( values, slopes, nt, i, interpolant );
}

template <class V, class D, class T>
double interp_pchip_value( const V& values, const D& slopes, usize_t nt, const T& query, usize_t index, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( values, nt, query, interpolant );
	else
		interp_linear_ascending( values, nt, query, interpolant );

	return interp_pchip( values, slopes, nt, index, interpolant );
}

// ------------------------------------------------------------------------

template <class T, class V, class D, class U, class S>
void interp_pchip_state_ts( const T& time, const V& values, const D& slopes, usize_t nt, const U& query, const S& state, usize_t ssize, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	for ( usize_t i = 0; i < ssize; ++i )
		state[i] = interp_pchip_ts( time, values, slopes, nt, i, interpolant );
}

template <class T, class V, class D, class U>
double interp_pchip_value_ts( const T& time, const V& values, const D& slopes, usize_t nt, const U& query, usize_t index, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	return interp_pchip_ts( time, values, slopes, nt, index, interpolant );
}

template <class T, class V, class D, class U>
double interp_pchip_value_tc( const T& time, const V& values, const D& slopes, usize_t nt, const U& query, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	return interp_pchip_tc( time, values, slopes, nt, interpolant );
}

OSD_NS_END_
