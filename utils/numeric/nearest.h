
//==================================================
// @title        nearest.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class V, class T>
usize_t nearest_ascending( const V& values, usize_t vsize, const T& query )
{
    if ( vsize <= 1 ) return 0;

    usize_t n = upper_bound( values, vsize, query );
    if ( (values[n]-query) > (query - values[n-1]) ) --n;
    return n;
}

template <class V, class T>
usize_t nearest_arithmetic( const V& values, usize_t vsize, const T& query )
{
    if ( vsize <= 1 ) return 0;

    auto r = std::round( (query-values[0]) / (values[1]-values[0]) );
    r = Drayn_CLAMP( r, 0.0, vsize-1.0 );
    return static_cast<usize_t>(r);
}

// ------------------------------------------------------------------------

template <class V, class T, class S>
void nearest_state( const V& values, usize_t vsize, const T& query, const S& state, usize_t ssize, bool is_arithmetic )
{
    const usize_t n = is_arithmetic? nearest_arithmetic(values,vsize,query) : nearest_ascending(values,vsize,query);
    for ( usize_t i = 0; i < ssize; ++i )
		state[i] = values(n,i);
}

template <class V, class T>
double nearest_value( const V& values, usize_t vsize, const T& query, usize_t index, bool is_arithmetic )
{
    const usize_t n = is_arithmetic? nearest_arithmetic(values,vsize,query) : nearest_ascending(values,vsize,query);
    return values(n,index);
}

// ------------------------------------------------------------------------

template <class T, class V, class U, class S>
void nearest_state_ts( const T& time, const V& values, usize_t nt, const U& query, const S& state, usize_t ssize, bool is_arithmetic )
{
    const usize_t n = is_arithmetic? nearest_arithmetic(time,nt,query) : nearest_ascending(time,nt,query);
    for ( usize_t i = 0; i < ssize; ++i )
		state[i] = values(n,i);
}

template <class T, class V, class U>
double nearest_value_ts( const T& time, const V& values, usize_t nt, const U& query, usize_t index, bool is_arithmetic )
{
    const usize_t n = is_arithmetic? nearest_arithmetic(time,nt,query) : nearest_ascending(time,nt,query);
    return values(n,index);
}

template <class T, class V, class U>
double nearest_value_tc( const T& time, const V& values, usize_t nt, const U& query, bool is_arithmetic )
{
    const usize_t n = is_arithmetic? nearest_arithmetic(time,nt,query) : nearest_ascending(time,nt,query);
    return values[n];
}

OSD_NS_END_
