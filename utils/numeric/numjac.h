
//==================================================
// @title        numjac.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * Evaluate Jacobian numerically.
 */

template <class _value, class _time = double>
class NumericJacobian
{
public:

	using value_type = _value;
	using time_type  = _time;
	using traits     = tp_traits<_value,_time>;
	using pool_type  = Pool<_value,_time>;

	using state_type     = typename traits::state_type;
	using jacobian_type  = typename traits::jacobian_type;

	// ----------  =====  ----------

	NumericJacobian()
		{ clear(); }
	NumericJacobian( usize_t nstates )
		{ resize(nstates); }

	inline usize_t    state_size () const { return m_before.size(); }
	inline value_type get_step   () const { return m_step; }

	void clear    ();
	void resize   ( usize_t nstates );
	void set_step ( value_type h );

	template <class S>
	void operator() ( const S& sys, time_type t, const state_type& x, const jacobian_type& J )
	{
		switch ( OSD_NUMJAC_METHOD )
		{
			case OSD_NUMJAC_FORWARD:
				_forward_approx(sys,t,x,J); break;

			case OSD_NUMJAC_CENTRAL:
				_central_approx(sys,t,x,J); break;

			default:
				throw std::runtime_error("Unknown method for numerical jacobian.");
		}
	}

private:

	/**
	 * Implementation using central approximation.
	 * Cost: 2*nstates calls to the derivative function.
	 */
	template <class S>
	void _central_approx( const S& sys, time_type t, const state_type& x, const jacobian_type& J );

	/**
	 * Implementation using forward approximation.
	 * Cost: nstates calls to the derivative function.
	 */
	template <class S>
	void _forward_approx( const S& sys, time_type t, const state_type& x, const jacobian_type& J );


	value_type              m_step;
	dr::vector<value_type>  m_before, m_after;
};



		/********************     **********     ********************/
		/********************     **********     ********************/



template <class V, class T>
void NumericJacobian<V,T>::clear()
{
	m_step   = 1e-6;
	m_before .clear();
	m_after  .clear();
}

// ------------------------------------------------------------------------

template <class V, class T>
void NumericJacobian<V,T>::resize( usize_t nstates )
{
	ASSERT_R( nstates > 1, "Number of states should be greater than 1." );

	m_before .create(nstates);
	m_after  .create(nstates);
}

// ------------------------------------------------------------------------

template <class V, class T>
void NumericJacobian<V,T>::set_step( value_type h )
{
	ASSERT_R( h >= dr::c_num<value_type>::eps, "Input step is too small." );
	m_step = h;
}

// ------------------------------------------------------------------------

template <class V, class T>
template <class S>
void NumericJacobian<V,T>::_central_approx( const S& sys, time_type t, const state_type& x, const jacobian_type& J )
{
	if ( sys.state_size() != state_size() )
	{
		DRAYN_INFO( "Size mismatch with the system, resizing members. Consider preallocating." );
		resize( sys.state_size() );
	}

	// Check dimensions
	const usize_t ns = state_size();
	ASSERT_R( x.size() == ns, "Size mismatch with input state." );
	ASSERT_R( J.dim(0) == ns, "Size mismatch with input jacobian (rows)." );
	ASSERT_R( J.dim(1) == ns, "Size mismatch with input jacobian (cols)." );

	value_type current;

	// Iterate on the rows of the Jacobian
	for ( usize_t r = 0; r < ns; ++r )
	{
		current = x[r];

		// Evaluate a little before
		x[r] = current - m_step;
		sys.derivative( t, x, m_before );

		// And after
		x[r] = current + m_step;
		sys.derivative( t, x, m_after );

		// Restore current value
		x[r] = current;

		// Compute the derivative with central approximation
		for ( usize_t c = 0; c < ns; ++c )
			J(r,c) = ( m_after[c] - m_before[c] ) / ( 2*m_step );
	}
}

// ------------------------------------------------------------------------

template <class V, class T>
template <class S>
void NumericJacobian<V,T>::_forward_approx( const S& sys, time_type t, const state_type& x, const jacobian_type& J )
{
	if ( sys.state_size() != state_size() )
	{
		DRAYN_INFO( "Size mismatch with the system, resizing members. Consider preallocating." );
		resize( sys.state_size() );
	}

	// Check dimensions
	const usize_t ns = state_size();
	ASSERT_R( x.size() == ns, "Size mismatch with input state." );
	ASSERT_R( J.dim(0) == ns, "Size mismatch with input jacobian (rows)." );
	ASSERT_R( J.dim(1) == ns, "Size mismatch with input jacobian (cols)." );

	value_type current;

	// Use the "before" vector to store the current derivative
	sys.derivative( t, x, m_before );

	// Iterate on the rows of the Jacobian
	for ( usize_t r = 0; r < ns; ++r )
	{
		current = x[r];

		// And after
		x[r] = current + m_step;
		sys.derivative( t, x, m_after );

		// Restore current value
		x[r] = current;

		// Compute the derivative with central approximation
		for ( usize_t c = 0; c < ns; ++c )
			J(r,c) = ( m_after[c] - m_before[c] ) / m_step;
	}
}
OSD_NS_END_
