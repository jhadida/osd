
//==================================================
// @title        upper_bound.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

OSD_NS_START_

/**
 * Replicate the standard algorithm, but using sequential access only (no random-access iterators).
 */
template <class V, class T>
usize_t upper_bound( const V& values, usize_t count, const T& query )
{
	usize_t first = 0;
	usize_t step  = 1;

	while ( count > 0 )
	{
		step = count/2;
		if ( !(query < values[first+step]) )
		{
			first += step+1;
			count -= step+1;
		}
		else count = step;
	}

	return first;
}

OSD_NS_END_
