
//==================================================
// @title        interp_linear.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

/**
 * NOTE:
 * These are quite minutiously implemented; modify with caution!
 */

#include <iostream>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * Result of linear interpolation.
 */
struct LinearInterpolant
{
	usize_t left,  right;
	double  wleft, wright;
};

// Output stream overload
inline std::ostream& operator<<( std::ostream& os, const LinearInterpolant& L )
{
	return os<< "[LinInterp] { " <<
		"L: (" << L.left  << "," << L.wleft  << "), " <<
		"R: (" << L.right << "," << L.wright << ") }\n";
}

// ------------------------------------------------------------------------

/**
 * Linear interpolation on a sorted (ascending) array of values.
 * The result contains the indices left and right such that:
 * values[left] <= query <= values[right]
 *
 * Undefined behaviour if values[left] == values[right].
 */
template <class V, class T>
void interp_linear_ascending( const V& values, usize_t vsize, const T& query, LinearInterpolant& res )
{
	dr::interval<T> interpolants;
	if ( vsize <= 1 )
	{
		res.left = res.right = 0;

		// NOTE: needs to be consistent with the following case right == 0
		res.wright = static_cast<double>((vsize == 0) || (query < values[0]));
	}
	else
	{
		res.right = upper_bound( values, vsize, query );
		if ( res.right == 0 )
		{
			res.left = 0;
			res.wright = 1.0;
			DRAYN_INFO("Interpolation before first value.");
		}
		else
		if ( res.right == vsize )
		{
			res.left = --res.right;
			res.wright = 0.0;
			DRAYN_INFO("Interpolation past last value.");
		}
		else
		{
			res.left = res.right-1;

			interpolants.assign( values[res.left], values[res.right] );
			DBG_ASSERT( interpolants, "Interpolants are too close to each other." );
			res.wright = interpolants.ratio( query );
			res.wright = Drayn_CLAMP( res.wright, 0.0, 1.0 );
		}
	}
	res.wleft = 1.0 - res.wright;
}

// ------------------------------------------------------------------------

/**
 * Idem, but optimized for arithmetic series.
 */
template <class V, class T>
void interp_linear_arithmetic( const V& values, usize_t vsize, const T& query, LinearInterpolant& res )
{
	double delta; iword_t left;
	if ( vsize <= 1 )
	{
		res.left = res.right = 0;

		// NOTE: needs to be consistent with the following case left < 0
		res.wright = static_cast<double>((vsize == 0) || (query < values[0]));
	}
	else
	{
		delta = values[1]-values[0];
		left  = dr::op_ifloor( (query-values[0]) / delta );

		if ( left < 0 )
		{
			res.left = res.right = 0;
			res.wright = 1.0;
			DRAYN_INFO("Interpolation before first value.");
		}
		else
		if ( left >= static_cast<iword_t>(vsize-1) )
		{
			res.left = res.right = vsize-1;
			res.wright = 0.0;
			DRAYN_INFO("Interpolation past last value.");
		}
		else
		{
			res.left   = left;
			res.right  = left+1;
			res.wright = ( query - values[left] ) / delta;
			res.wright = std::max( res.wright, 1.0 );
		}
	}
	res.wleft = 1.0 - res.wright;
}

// ------------------------------------------------------------------------

template <class V, class T, class S>
void interp_linear_state( const V& values, usize_t vsize, const T& query, const S& state, usize_t ssize, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( values, vsize, query, interpolant );
	else
		interp_linear_ascending( values, vsize, query, interpolant );

	for ( usize_t i = 0; i < ssize; ++i )
		state[i] =
			interpolant.wleft  * values( interpolant.left,  i ) +
			interpolant.wright * values( interpolant.right, i ) ;
}

template <class V, class T>
double interp_linear_value( const V& values, usize_t vsize, const T& query, usize_t index, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( values, vsize, query, interpolant );
	else
		interp_linear_ascending( values, vsize, query, interpolant );

	return
		interpolant.wleft  * values( interpolant.left,  index ) +
		interpolant.wright * values( interpolant.right, index ) ;
}

// ------------------------------------------------------------------------

template <class T, class V, class U, class S>
void interp_linear_state_ts( const T& time, const V& values, usize_t nt, const U& query, const S& state, usize_t ssize, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	for ( usize_t i = 0; i < ssize; ++i )
		state[i] =
			interpolant.wleft  * values( interpolant.left,  i ) +
			interpolant.wright * values( interpolant.right, i ) ;
}

template <class T, class V, class U>
double interp_linear_value_ts( const T& time, const V& values, usize_t nt, const U& query, usize_t index, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	return
		interpolant.wleft  * values( interpolant.left,  index ) +
		interpolant.wright * values( interpolant.right, index ) ;
}

template <class T, class V, class U>
double interp_linear_value_tc( const T& time, const V& values, usize_t nt, const U& query, bool is_arithmetic )
{
	LinearInterpolant interpolant;

	if ( is_arithmetic )
		interp_linear_arithmetic( time, nt, query, interpolant );
	else
		interp_linear_ascending( time, nt, query, interpolant );

	return
		interpolant.wleft  * values[ interpolant.left  ] +
		interpolant.wright * values[ interpolant.right ] ;
}

OSD_NS_END_
