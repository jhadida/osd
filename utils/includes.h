#ifndef OSD_UTILS_H_INCLUDED
#define OSD_UTILS_H_INCLUDED

//==================================================
// @title        includes.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include "info.h"

#include "algebra/iter_apply.h"
#include "algebra/weighted_sum.h"
#include "algebra/lu.h"

#include "memory/buffer.h"
#include "memory/block.h"
#include "memory/pool.h"
#include "memory/downsampler.h"
#include "memory/pool_utils.h"

#include "numeric/upper_bound.h"
#include "numeric/nearest.h"
#include "numeric/interp_linear.h"
#include "numeric/interp_pchip.h"
#include "numeric/numjac.h"

#include "timecourse.h"
#include "timeseries.h"

#endif
