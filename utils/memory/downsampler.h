
//==================================================
// @title        downsampler.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <deque>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time = double>
struct PoolDownsampler
{
	using value_type  = _value;
	using time_type   = _time;
	using pool_type   = Pool<_value,_time>;
	using block_type  = typename pool_type::block_type;
	using slot_type   = dr::slot<pool_type>;

	// ----------  =====  ----------

	slot_type subscriber;
	std::deque<block_type> dspool;
	usize_t step, length;

	// ----------  =====  ----------

	PoolDownsampler()
		{ clear(); }
	PoolDownsampler( pool_type& src, usize_t k )
		{ assign(src,k); }

	inline bool active () const { return step; }
	inline bool empty  () const { return length==0; }

	void clear();
	void assign( pool_type& src, usize_t k );

	void callback( pool_type& src ); // triggered by new_block events
	void finalise( const pool_type& src ); // to be called before export
};

// ------------------------------------------------------------------------

template <class V, class T>
void PoolDownsampler<V,T>::clear()
{
	subscriber.reset();
	dspool.clear();
	step = length = 0;
}

// ------------------------------------------------------------------------

template <class V, class T>
void PoolDownsampler<V,T>::assign( pool_type& src, usize_t k )
{
	dspool.clear();
	subscriber = src.event_new_block.subscribe(
		[this]( pool_type& p ){ this->callback(p); }
	);
	step = k; length = 0;
}

// ------------------------------------------------------------------------

template <class V, class T>
void PoolDownsampler<V,T>::callback( pool_type& src )
{
	while ( src.n_blocks() > 2 ) // always leave two blocks in front
	{
		auto block = src.block(0); // local shallow copy
		ASSERT_R( block.downsample(step), "[PoolDownsampler.callback] Could not downsample block." );
		dspool.push_back(block);
		length += block.size();
		src.pop_front();
	}
}

// ------------------------------------------------------------------------

template <class V, class T>
void PoolDownsampler<V,T>::finalise( const pool_type& src )
{
	for ( usize_t i = src.n_blocks(); i > 0; --i )
	{
		auto block = src.block(i-1); // local shallow copy
		switch (i)
		{
		case 1: // last block might not be full, make sure we get the last timepoint
			ASSERT_R( block.downsample(step,src.index() % src.block_size()),
				"[PoolDownsampler.finalise] Could not downsample last block." );
			break;
		default:
			ASSERT_R( block.downsample(step), "[PoolDownsampler.finalise] Could not downsample block." );
			break;
		}

		dspool.push_back(block);
		length += block.size();
	}
}

OSD_NS_END_
