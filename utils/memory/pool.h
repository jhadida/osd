
//==================================================
// @title        pool.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <deque>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time = double>
class Pool
{
public:

	typedef Pool<_value,_time>   self;

	using value_type   = _value;
	using time_type    = _time;
	using block_type   = Block<_value,_time>;
	using state_type   = typename block_type::state_type;
	using signal_type  = dr::signal<self>;

	signal_type event_new_block;

	// ----------  =====  ----------

	Pool()
		{ clear(); }
	Pool( usize_t state_size, usize_t block_size = OSD_BLOCK_SIZE )
		{ clear(); init( state_size, block_size ); }
	Pool( const std::deque<block_type>& blocks, usize_t index )
		{ clear(); init( blocks, index ); }


	// Initialize/delete storage
	void clear ();
	void init  ( usize_t state_size, usize_t block_size = OSD_BLOCK_SIZE );
	bool init  ( const std::deque<block_type>& blocks, usize_t index );


	// Dimensions
	inline usize_t index      () const { return m_index; }
	inline usize_t n_blocks   () const { return m_blocks.size(); }
	inline usize_t block_size () const { return m_block_size; }
	inline usize_t state_size () const { return m_state_size; }

	inline usize_t capacity   () const { return n_blocks()*block_size(); }
	inline usize_t size       () const { return capacity()? (m_index+1) : 0; }

	inline usize_t n_times    () const { return size(); }
	inline usize_t n_states   () const { return size(); }
	inline usize_t n_signals  () const { return state_size(); }
	inline usize_t n_channels () const { return state_size(); }

	// Validity
	inline bool    empty() const { return size()==0; }
	inline bool    valid() const { return state_size()*block_size(); }
	inline operator bool() const { return valid() && !empty(); }


	// Access blocks
	inline const block_type& block( usize_t n ) const
		{ return m_blocks.at(n); }

	// Pop blocks
	void pop_front()
	{
		ASSERT_THROW( n_blocks() > 2, "Can only pop inactive blocks." );
		m_blocks.pop_front();
		m_index -= m_block_size;
	}

	// Note: pushing blocks is a bit too fiddly


	// Access states
	inline time_type& time ( usize_t n ) const { return block( n/m_block_size ).time ( n % m_block_size ); }
	inline time_type& time ()            const { return time(m_index); }

	inline const state_type& state ( usize_t n ) const { return block( n/m_block_size ).state( n % m_block_size ); }
	inline const state_type& state ()            const { return state(m_index); }

	inline value_type& value ( usize_t n, usize_t i ) const { return block( n/m_block_size ).value( n % m_block_size, i ); }
	inline value_type& value ( usize_t i )            const { return value(m_index,i); }


	// operator[] gives the corresponding TIME
    inline time_type& operator[] ( usize_t n ) const { return time(n); }

    // operator() gives the corresponding VALUE
    inline value_type& operator() ( usize_t n, usize_t i ) const { return value(n,i); }


	// Increment/decrement
	void increment()
	{
		if ( ++m_index >= capacity() )
		{
			m_blocks.emplace_back( m_state_size, m_block_size );
			event_new_block.publish( *this );
		}
	}
	inline void increment( usize_t k )
		{ for( ; k; --k ) increment(); }

	inline void decrement()
		{ if ( m_index > 0 ) --m_index; }
	inline void decrement( usize_t k )
		{ for( ; k; --k ) decrement(); }

	inline void reset() { m_index = 0; }

private:

	usize_t  m_index;
	usize_t  m_block_size, m_state_size;
	std::deque<block_type> m_blocks;
};

// ------------------------------------------------------------------------

template <class V, class T>
void Pool<V,T>::clear()
{
	m_index = m_state_size = 0;
	m_block_size = 1;
	m_blocks.clear();
}

// ------------------------------------------------------------------------

template <class V, class T>
void Pool<V,T>::init( usize_t state_size, usize_t block_size )
{
	ASSERT_THROW( block_size >= 2, "Block-size should be greater than 2." );
	if ( (state_size != m_state_size) || (block_size != m_block_size) )
	{
		clear();

		m_state_size = state_size;
		m_block_size = block_size;

		m_blocks.emplace_back( state_size, block_size );
	}
	else reset();
}

// ------------------------------------------------------------------------

template <class V, class T>
bool Pool<V,T>::init( const std::deque<block_type>& blocks, usize_t index )
{
	// make sure it's not empty
	REJECT_RF( blocks.empty(), "External initialisation with empty container is not allowed." );

	m_block_size = blocks.front().size();
	m_state_size = blocks.front().state_size();

	ASSERT_RF( m_block_size*m_state_size > 0, "Block-size and state-size should be positive." );
	ASSERT_RF( m_block_size >= 2, "Block-size should be greater than 2." );

	// make sure they all have the same size
	for ( auto& b: blocks ) ASSERT_RF( b.size() == m_block_size, "Non-uniform block-sizes." );

	// make sure index is within container
	ASSERT_RF( index/m_block_size < blocks.size(), "Index out of bounds." );

	// everything is fine
	m_index  = index;
	m_blocks = blocks;

	return true;
}

OSD_NS_END_
