
//==================================================
// @title        buffer.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <stdexcept>



        /********************     **********     ********************/
        /********************     **********     ********************/



OSD_NS_START_

/**
 * A circular buffer is a data container with fixed capacity but variable length
 * (the front and back of the container can slide within a fixed-size frame), and
 * index 0 corresponds to the front of the container.
 *
 * Below is a circular buffer of length 5, offset 8 and capacity 11.
 *  "mid" = memory index
 *  "vid" = virtual index
 *
 * mid:  0   1   2   3   ...                     10
 *     +---+---+---+---+---+---+---+---+---+---+---+  `capacity` columns
 *     |   | 0 |   |   |   |   |   |   |   |   |   |
 *
 *                      ... `state_size` rows
 *     |   | 7 |   |   |   |   |   |   |   |   |   |
 *     |   | 2 |   |   |   |   |   |   |   |   |   |
 *     +---+---+---+---+---+---+---+---+---+---+---+
 *             >                       <
 *             Back                    Front
 * vid:  3   4                           0   1   2
 *
 */
template <class _value, class _time = double>
class CircularBuffer
{
public:

    using self        = CircularBuffer<_value,_time>;
    using time_type   = _time;
    using value_type  = _value;
	using state_type  = typename tp_traits< value_type, time_type >::state_type;

	// ----------  =====  ----------

	CircularBuffer()
        { clear(); }
    CircularBuffer( usize_t state_size, usize_t capacity )
        { init(state_size,capacity); }

	void clear ();
	void init  ( usize_t state_size, usize_t capacity );

	// Dimensions
	inline usize_t length     () const { return m_length; }
    inline usize_t size       () const { return m_length; }
	inline usize_t n_states   () const { return size(); }
	inline usize_t n_times    () const { return size(); }

	inline usize_t state_size () const { return m_state_size; }
	inline usize_t n_signals  () const { return state_size(); }
	inline usize_t n_channels () const { return state_size(); }

    // Empty/full
    inline usize_t capacity   () const { return m_times.size(); }
    inline bool    empty      () const { return size()==0; }
    inline bool    full       () const { return size()==capacity(); }


	// Forward data access
	inline time_type&  time  ( usize_t n ) const { return m_times[index(n)]; }
	inline value_type& value ( usize_t n, usize_t i ) const { return m_states[ index(n)*m_state_size + i ]; }

    const state_type& state( usize_t n ) const
    {
        thread_local state_type local_state;
        local_state.assign( &m_states[index(n)*m_state_size], m_state_size );
        return local_state;
    }

    inline time_type& front_time() const { return time(0); }
    inline value_type& front_value( usize_t i ) const { return value(0,i); }
    inline const state_type& front_state() const { return state(0); }

    inline time_type& back_time() const { return time(m_length-1); }
    inline value_type& back_value( usize_t i ) const { return value(m_length-1,i); }
    inline const state_type& back_state() const { return state(m_length-1); }


    // Backward data access
	inline time_type&  rtime  ( usize_t n ) const { return time(m_length-1-n); }
	inline value_type& rvalue ( usize_t n, usize_t i ) const { return value(m_length-1-n,i); }
    inline const state_type& rstate( usize_t n ) const { return rstate(m_length-1-n); }

    inline time_type& front_rtime() const { return back_time(); }
    inline value_type& front_rvalue( usize_t i ) const { return back_value(i); }
    inline const state_type& front_rstate() const { return back_state(); }

    inline time_type& back_rtime() const { return front_time(); }
    inline value_type& back_rvalue( usize_t i ) const { return front_value(i); }
    inline const state_type& back_rstate() const { return front_state(); }


	// operator[] gives the corresponding TIME
    inline time_type& operator[] ( usize_t n ) const { return time(n); }

    // operator() gives the corresponding VALUE
    inline value_type& operator() ( usize_t n, usize_t i ) const { return value(n,i); }

    self& pop  (); // remove front
    self& push ( time_type t, const state_type& x ); // push back

protected:

    usize_t index( usize_t n ) const
    {
        ASSERT_THROW( n < this->m_length, "[osd.CircularBuffer] Index out of bounds." );
        return (this->m_offset + n) % capacity();
    }

	usize_t                 m_state_size, m_offset, m_length;
	dr::vector<time_type>   m_times;
	dr::vector<value_type>  m_states;

};

// ------------------------------------------------------------------------

template <class V, class T>
void CircularBuffer<V,T>::clear()
{
	m_state_size = m_offset = m_length = 0;
	m_times  .clear();
	m_states .clear();
}

// ------------------------------------------------------------------------

template <class V, class T>
void CircularBuffer<V,T>::init( usize_t state_size, usize_t capacity )
{
	ASSERT_R( state_size*capacity, "Can't initialize with a null size!" );

    clear(); // reset container
	m_state_size = state_size;

	m_times  .create( capacity, T(0) );
	m_states .create( state_size*capacity, V(0) );
}

// ------------------------------------------------------------------------

template <class V, class T>
auto CircularBuffer<V,T>::pop() -> self&
{
    // shift front and decrease size
    if ( m_length > 0 )
    {
        --m_length;
        m_offset = (m_offset+1) % capacity();
    }

    return *this;
}

// ------------------------------------------------------------------------

template <class V, class T>
auto CircularBuffer<V,T>::push( time_type t, const state_type& x ) -> self&
{
    if ( m_length < capacity() )
        ++m_length; // increase size
    else
        m_offset = (m_offset+1) % capacity(); // shift front

    time(0) = t;
    state(0).copy(x);

    return *this;
}

OSD_NS_END_
