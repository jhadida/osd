
//==================================================
// @title        pool_utils.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * fetch  : fetch the latest/a given pool record and bind the input timepoint
 * commit : copy input timepoint to the corresponding record
 */

template <class _pool, class _timepoint>
inline void pool_fetch_timepoint( const _pool& P, _timepoint& tp )
{
	pool_fetch_timepoint( P, P.index(), tp );
}

template <class _pool, class _timepoint>
void pool_fetch_timepoint( const _pool& P, usize_t index, _timepoint& tp )
{
	DBG_ASSERT_R( index <= P.index(),
		"Index out of range ( " DPus" > " DPus " ).", index, P.index() );
	DBG_ASSERT_R( tp.state_size() == P.state_size(),
		"Input size mismatch ( " DPus" != " DPus " ).", tp.state_size(), P.state_size() );

	tp.index = index;
	tp.t = P.time (index);
	tp.x = P.state(index);
}

template <class _pool, class _timepoint>
void pool_commit_timepoint( const _pool& P, const _timepoint& tp )
{
	const usize_t index = tp.index;

	DBG_ASSERT_R( index <= P.index(),
		"Index out of range ( " DPus" > " DPus " ).", index, P.index() );
	DBG_ASSERT_R( tp.state_size() == P.state_size(),
		"Input size mismatch ( " DPus" != " DPus " ).", tp.state_size(), P.state_size() );

	// Nothing to do for state thanks to dr::array
	P.time(index) = tp.t;
}

OSD_NS_END_

/**
 * DEPRECATED (JH, 2016 Feb 15)
 * Export times and states respectively to a vector & matrix.
 */
// template <class _pool, class _vector, class _matrix>
// void pool_export( const _pool& P, const _vector& times, const _matrix& states, bool transpose = false )
// {
// 	const usize_t psize = P.size();
//
// 	ASSERT_R( states.ndims() == 2,
// 		"Input states should be a matrix." );
// 	ASSERT_R( times.size() == psize,
// 		"Input size mismatch (times " DPus " != pool " DPus ").", times.size(), psize );
//
// 	if ( transpose )
// 	{
// 		usize_t nrows = psize;
// 		usize_t ncols = P.state_size();
//
// 		ASSERT_R( states.nrows() == nrows,
// 			"Bad number of rows (received: " DPus ", expected: " DPus ").", states.nrows(), nrows );
// 		ASSERT_R( states.ncols() == ncols,
// 			"Bad number of cols (received: " DPus ", expected: " DPus ").", states.ncols(), ncols );
//
// 		for ( usize_t i = 0; i < nrows; ++i )
// 		{
// 			states.row(i).copy( P.state(i) );
// 			times[i] = P.time(i);
// 		}
// 	}
// 	else
// 	{
// 		usize_t nrows = P.state_size();
// 		usize_t ncols = psize;
//
// 		ASSERT_R( states.nrows() == nrows,
// 			"Bad number of rows (received: " DPus ", expected: " DPus ").", states.nrows(), nrows );
// 		ASSERT_R( states.ncols() == ncols,
// 			"Bad number of cols (received: " DPus ", expected: " DPus ").", states.ncols(), ncols );
//
// 		for ( usize_t i = 0; i < ncols; ++i )
// 		{
// 			states.col(i).copy( P.state(i) );
// 			times[i] = P.time(i);
// 		}
// 	}
// }
