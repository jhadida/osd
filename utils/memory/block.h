
//==================================================
// @title        block.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <stdexcept>
#include <vector>


		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _value, class _time = double>
class Block
{
public:

	typedef Block<_value,_time> self;

	using time_type  = _time;
	using value_type = _value;
	using state_type = typename tp_traits< value_type, time_type >::state_type;

	// ----------  =====  ----------

	Block()
		{ clear(); }
	Block( usize_t state_size, usize_t block_size )
		{ alloc( state_size, block_size ); }


	void clear ();
	void alloc ( usize_t state_size, usize_t block_size );


	// Dimensions
	inline usize_t size       () const { return m_times.size(); }
	inline usize_t block_size () const { return size(); }
	inline usize_t n_states   () const { return size(); }
	inline usize_t n_times    () const { return size(); }

	inline usize_t state_size () const { return m_state_size; }
	inline usize_t n_signals  () const { return state_size(); }
	inline usize_t n_channels () const { return state_size(); }

	// Access data
	inline time_type&  time  ( usize_t n ) const { return m_times[n]; }
	inline value_type& value ( usize_t n, usize_t i ) const { return m_states[ n*m_state_size + i ]; }

	const state_type& state( usize_t n ) const
    {
        thread_local state_type local_state;
        local_state.assign( &m_states[n*m_state_size], m_state_size );
        return local_state;
    }

	// operator[] gives the corresponding TIME
    inline time_type& operator[] ( usize_t n ) const { return time(n); }

    // operator() gives the corresponding VALUE
    inline value_type& operator() ( usize_t n, usize_t i ) const { return value(n,i); }

	// reduce sampling rate by selecting the first of k timepoints sequentially
	bool downsample( usize_t k, usize_t last = 0 );

private:

	usize_t                 m_state_size;
	dr::vector<time_type>   m_times;
	dr::vector<value_type>  m_states;
};

// ------------------------------------------------------------------------

template <class V, class T>
void Block<V,T>::clear()
{
	m_state_size = 0;
	m_times  .clear();
	m_states .clear();
}

// ------------------------------------------------------------------------

template <class V, class T>
void Block<V,T>::alloc( usize_t state_size, usize_t block_size )
{
	ASSERT_R( state_size*block_size, "Can't initialize with a null size!" );

	m_state_size = state_size;
	m_times  .create( block_size );
	m_states .create( state_size*block_size );
}

// ------------------------------------------------------------------------

template <class V, class T>
bool Block<V,T>::downsample( usize_t step, usize_t last )
{
	ASSERT_RF( step > 0, "Step should be non-zero." );
	ASSERT_RF( step < size(), "Step should be less than the block-size." );

	// last timepoint
	if ( last == 0 ) last = size()-1;
	ASSERT_RF( last < size(), "Last required timepoint out of bounds." );

	// selected timepoints
	std::vector<usize_t> sel;
	for ( usize_t i = 0; i <= last; i += step ) sel.push_back(i);
	if ( sel.back() != last ) sel.push_back(last);

	// allocate new data
	const usize_t n = sel.size();
	dr::vector<time_type>  tnew(n);
	dr::vector<value_type> snew(n*m_state_size);

	// downsample
	for ( usize_t i = 0; i < n; ++i )
	{
		tnew[i] = time(sel[i]);
		for ( usize_t j = 0; j < m_state_size; ++j )
			snew[ i*m_state_size + j ] = value(sel[i],j);
	}

	// assign members
	m_times  = tnew;
	m_states = snew;

	return true;
}

OSD_NS_END_
