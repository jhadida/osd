
//==================================================
// @title        adaptive_step_size.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <string>
#include <sstream>



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _stepper, class _controller = Controller_Default>
struct Integrator_Adaptive_StepSize
	: public Integrator<_stepper>
{
	using self   = Integrator_Adaptive_StepSize<_stepper,_controller>;
	using parent = Integrator<_stepper>;

	using stepper_type     = _stepper;
	using controller_type  = _controller;
	using system_type      = typename parent::system_type;
	using problem_type     = typename parent::problem_type;
	using property_type    = typename parent::property_type;
	using idata_type       = typename parent::idata_type;

	static_assert( stepper_type::estimates_error::value,
		"This integrator requires error estimations for step-size adaptation." );

	// ----------  =====  ----------

	Integrator_Adaptive_StepSize()
		: m_controller( stepper_type::order::value ) {}

	void integrate( system_type& sys, problem_type& problem, property_type& prop )
	{
		// Used to warn about time-step over/under-flow
		std::stringstream xflow_stream;
		std::string xflow_str;

		// Initialize and start
		prop.step.num_steps = 0;
		ASSERT_R( this->init(sys,problem,prop), "Initialisation failed." );
		prop.event.after_init.publish( idata() );

		while ( ! idata().done() )
		{
			this->fetch_next();

			// Make sure time-step is within allowed bounds
			if( idata().cur.dt > prop.step.max_step || idata().cur.dt < prop.step.min_step )
			{
				idata().cur.dt = dr::op_clamp( idata().cur.dt, prop.step.min_step, prop.step.max_step );
				xflow_stream << idata().cur.index << ", ";
			}

			// Adjust the timestep to finish exactly at t1
			idata().adjust_time_step();

			// Try a step and update the current time-step,
			// as long as the error doesn't meet the accuracy requirements.
			while ( true )
			{
				prop.event.before_step.publish( idata() );
				idata().err = this->stepper.step( idata(), prop );

				if ( m_controller.accept(idata().err) ) // step accepted, update next time-step
				{
					idata().next.dt = idata().cur.dt * m_controller.scaling_factor( idata().err );
					idata().next.t  = idata().cur.t + idata().cur.dt;

					m_controller.update( idata().err ); break;
				}
				else // step rejected, update current time-step
				{
					m_controller.update( idata().err );
					idata().cur.dt = idata().cur.dt * m_controller.scaling_factor( idata().err );
				}
			}

			// Update time-properties
			++prop.step.num_steps;
			prop.event.after_step.publish( idata() );

			this->commit_next();
			prop.event.after_commit.publish( idata() );
		}

		// // uncomment this to show indices
		// if ( xflow_stream.tellp() )
		// {
		// 	xflow_str = xflow_stream.str();
		// 	xflow_str.pop_back();
		// 	xflow_str.pop_back(); // remove last separator
		// 	osd_info( "The following timepoint(s) (numbered from 0) correspond to time-step over/under-flow:\n%s", xflow_str.c_str() );
		// }
	}


private:

	// Writing this-> all the time is annoying
	inline idata_type& idata() { return this->m_idata; }

	controller_type m_controller;
};

OSD_NS_END_
