
//==================================================
// @title        ross.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>
#include <algorithm>
#include <stdexcept>

#define OSD_CONTROLLER_ROSS_MIN_ERROR   1e-2
#define OSD_CONTROLLER_ROSS_MAX_REJECT  25



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

struct Controller_Ross
	: public Controller
{
	double alpha, err_old, scale_old;
	double min_scale, max_scale, safe_scale;

	uword_t reject_count;
	bool first_step;

	// ----------  =====  ----------

	Controller_Ross( unsigned order = 4 )
	{
		if ( order == 0 )
		{
			DRAYN_WARN( "Order cannot be 0, setting to 1 instead." );
			order = 1;
		}

		alpha = 1.0/order;

		safe_scale = 0.80; // 0.95
		min_scale  = 0.10; // 0.2
		max_scale  = 5.00;

		err_old      = OSD_CONTROLLER_ROSS_MIN_ERROR;
		scale_old    = 1;
		reject_count = 0;
		first_step   = true;
	}

	bool   update         ( double err );
	double scaling_factor ( double err );
};

OSD_NS_END_
