
//==================================================
// @title        default.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================

#include <cmath>
#include <algorithm>
#include <stdexcept>

#define OSD_CONTROLLER_DEFAULT_MIN_ERROR   1e-4
#define OSD_CONTROLLER_DEFAULT_MAX_REJECT  25



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

/**
 * dt_{n+1} / dt_n = safe_scale * pow(err,-alpha) * pow(err,beta)
 *
 * Recommended values:
 * 	beta = 0.4/k, alpha = 0.7/k
 * where k is the order of the stepper.
 */
struct Controller_Default
	: public Controller
{
	double alpha, beta;
	double min_scale, max_scale, safe_scale;
	double err_old;

	uword_t reject_count;

	// ----------  =====  ----------

	Controller_Default( unsigned order = 1 )
	{
		if ( order == 0 )
		{
			DRAYN_WARN( "Order cannot be 0, setting to 1 instead." );
			order = 1;
		}

		beta  = 0.0/order;
		alpha = 1.0/order - 0.75*beta;

		safe_scale = 0.80;
		min_scale  = 0.10;
		max_scale  = 5.00;

		err_old      = OSD_CONTROLLER_DEFAULT_MIN_ERROR;
		reject_count = 0;
	}

	bool   update         ( double err );
	double scaling_factor ( double err );
};

OSD_NS_END_
