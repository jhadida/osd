
//==================================================
// @title        ross.cpp
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

bool Controller_Ross::update( double err )
{
	if ( this->accept(err) )
	{
		reject_count = 0;
		return true;
	}
	else
	if ( OSD_CONTROLLER_ROSS_MAX_REJECT && (++reject_count > OSD_CONTROLLER_ROSS_MAX_REJECT) )
		throw std::runtime_error("Maximum number of reject exceeded.");

	return false;
}

// ------------------------------------------------------------------------

double Controller_Ross::scaling_factor( double err )
{
	bool   acc = this->accept(err);
	double lmx = ( !acc || reject_count ) ? 1.0 : max_scale; // no increase if reject
	double fac = ( acc && !first_step && err*err_old >= dr::c_num<double>::eps ) ?
		std::min( scale_old*std::pow(err/err_old,-alpha), 1.0 ) : 1.0;

	double scale = ( err < dr::c_num<double>::eps ) ?
		lmx : dr::op_clamp( safe_scale*std::pow(err,-alpha)*fac, min_scale, lmx );

	// update error and scale
	if (acc)
	{
		err_old    = std::max( err, OSD_CONTROLLER_DEFAULT_MIN_ERROR );
		first_step = false;
		scale_old  = scale;
	}

	return scale;
}

OSD_NS_END_
