
//==================================================
// @title        fixed_num_steps.h
// @author       Jonathan Hadida
// @contact      Jhadida [at] fmrib.ox.ac.uk
//==================================================



		/********************     **********     ********************/
		/********************     **********     ********************/



OSD_NS_START_

template <class _stepper>
struct Integrator_Fixed_NumSteps
	: public Integrator<_stepper>
{
	using self   = Integrator_Fixed_NumSteps<_stepper>;
	using parent = Integrator<_stepper>;

	using stepper_type   = _stepper;
	using system_type    = typename parent::system_type;
	using problem_type   = typename parent::problem_type;
	using property_type  = typename parent::property_type;
	using idata_type     = typename parent::idata_type;

	// ----------  =====  ----------

	void integrate( system_type& sys, problem_type& prob, property_type& prop )
	{
		// Compute timestep given time-interval and number of steps
		ASSERT_R( prop.step.num_steps > 1, "Number of steps must be at least 2." );

		prop.step.init_step = prop.step.min_step = prop.step.max_step =
			prob.time_interval() / (prop.step.num_steps - 1);
		prop.step.num_steps = 0;

		// Initialise
		ASSERT_R( this->init(sys,prob,prop), "Initialisation failed." );
		prop.event.after_init.publish( idata() );

		while ( ! idata().done() )
		{
			this->fetch_next();

			// Here we could adjust cur.dt to finish exactly at t1

			prop.event.before_step.publish( idata() );
			idata().err = this->stepper.step( idata(), prop );

			// Here we could adjust the next timestep using the error

			// update time-properties
			idata().next.t  = idata().t_start() + (++prop.step.num_steps) * idata().cur.dt;
			idata().next.dt = idata().cur.dt;

			// nothing to do before publishing this event for fixed-step integration
			prop.event.after_step.publish( idata() );

			this->commit_next();
			prop.event.after_commit.publish( idata() );
		}
	}

private:

	// Writing this-> all the time is annoying
	inline idata_type& idata() { return this->m_idata; }
};

OSD_NS_END_
